package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.ui.Button;
import com.oddrocket.orbitexpress.ui.ButtonRenderBack;

import android.graphics.Canvas;

public class HelpScreen extends MenuScreen {
    private static final int ACTION_SYSTEM_BACK = 1;

    HelpScreen(GameManager game) {
        super(game);
        setTitle("help");

        Button b = new Button(canvasWidth - (m_roundButtonSize + m_xoffset), m_xoffset, m_roundButtonSize, m_roundButtonSize, ACTION_SYSTEM_BACK);
        b.setRender(new ButtonRenderBack());
        m_ui.addComponent(b);
    }

    @Override
    public void update(float timeDelta) {
        // TODO Auto-generated method stub
    }

    @Override
    public void present(Canvas canvas, float timeDelta) {
        super.present(canvas, timeDelta);

        String text;
        int ty = m_textSize * 3;
        m_headerPaint.setTextSize(m_textSizeHalf);

        text = "Get your rocket to the destination planet";
        canvas.drawText(text, m_xoffset, ty, m_headerPaint);
        ty = ty + m_textSize;

        text = "indicated by the red ring.";
        canvas.drawText(text, m_xoffset, ty, m_headerPaint);
        ty = ty + m_textSize;

        ty = ty + m_textSize;

        text = "Adjust the power and angle of launch";
        canvas.drawText(text, m_xoffset, ty, m_headerPaint);
        ty = ty + m_textSize;

        text = "before sending the rocket on its way.";
        canvas.drawText(text, m_xoffset, ty, m_headerPaint);
        ty = ty + m_textSize;

        /*
        text = "help help help";
        canvas.drawText(text, m_xoffset, ty, m_headerPaint);
        ty = ty + m_textSize;

        text = "help help help";
        canvas.drawText(text, m_xoffset, ty, m_headerPaint);
        ty = ty + m_textSize;
        */

        m_headerPaint.setTextSize(m_textSize);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    public void uiAction(int action, int id, int value) {
        if (id == ACTION_SYSTEM_BACK) {
            m_game.setScreen(new MainMenuScreen(m_game));
        }
    }

}
