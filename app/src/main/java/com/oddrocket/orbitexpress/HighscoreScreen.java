package com.oddrocket.orbitexpress;

import java.util.Vector;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.Highscores;
import com.oddrocket.orbitexpress.ui.Button;
import com.oddrocket.orbitexpress.ui.ButtonRenderBack;

import android.graphics.Canvas;
import android.graphics.Rect;

public class HighscoreScreen extends MenuScreen {
	private static final int ACTION_SYSTEM_BACK = 10;
	private static final int ACTION_1 = 1;
	private static final int ACTION_2 = 2;
	private static final int ACTION_3 = 3;
	private static final int ACTION_4 = 4;

	private final Vector<Button> m_buttons = new Vector<>();
	private int m_level = 1;
	
	HighscoreScreen(GameManager game) {
		super(game);
		setTitle("highscore");
		
		Button b = new Button(canvasWidth - (m_roundButtonSize + m_xoffset), m_xoffset, m_roundButtonSize, m_roundButtonSize, ACTION_SYSTEM_BACK);
		b.setRender(new ButtonRenderBack());
		m_ui.addComponent(b);
		
		String text = "select system";
		Rect r = new Rect();
		m_headerPaint.setTextSize(m_textSizeHalf);
		m_headerPaint.getTextBounds(text, 0, text.length(), r);
		m_headerPaint.setTextSize(m_textSize);
		
		int buttonwidth = (int) (m_textSize * 0.75);
		int buttonstarty = m_textSize * 3 - buttonwidth;
		int buttonstartx = m_xoffset * 3 + r.right;
		int xinc = buttonwidth * 2;
		
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_1);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("1");
		b.setTypeface(m_typeface);
		m_ui.addComponent(b);
		m_buttons.add(b);
		
		buttonstartx = buttonstartx + xinc;
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_2);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("2");
		b.setTypeface(m_typeface);
		m_ui.addComponent(b);
		m_buttons.add(b);

		buttonstartx = buttonstartx + xinc;
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_3);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("3");
		b.setTypeface(m_typeface);
		m_ui.addComponent(b);
		m_buttons.add(b);
		
		buttonstartx = buttonstartx + xinc;
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_4);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("4");
		b.setTypeface(m_typeface);
		m_ui.addComponent(b);
		m_buttons.add(b);
		
		m_buttons.get(0).setSelected(true);
		
	}

	@Override
	public void update(float timeDelta) {
		// TODO Auto-generated method stub
	}

	private synchronized void setLevel(int level) {
		m_level = level;
	}
	
	private synchronized int getLevel() {
		return m_level;
	}
	
	@Override
	public void present(Canvas canvas, float timeDelta) {
		super.present(canvas, timeDelta);
		
		Highscores hs = m_game.getHighscores();
		String text;
		int ty;
		int level = getLevel();
		m_headerPaint.setTextSize(m_textSizeHalf);
		
		ty = m_textSize * 3;
		text = "select system";
		canvas.drawText(text, m_xoffset, ty, m_headerPaint);
		
		ty = m_textSize * 4 + m_textSizeHalf;
		
		if (hs.isLevelComplete(level)) {
			text = "Shortest total distance:";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			ty = ty + m_textSizeHalf;
			text = hs.getLevelScoreMin1(level) + " km";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			
			/*
			ty = ty + theight + hheight;
			text = "Shortest time:";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			ty = ty + theight;
			text = hs.getLevelScoreMin2(level) + " days";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			*/
			
			ty = ty + m_textSizeHalf + m_textSizeHalf;
			text = "Longest total distance:";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			ty = ty + m_textSizeHalf;
			text = hs.getLevelScoreMax1(level) + " km";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			
			/*
			ty = ty + theight + hheight;
			text = "Longest time:";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			ty = ty + theight;
			text = hs.getLevelScoreMax2(level) + " days";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
			*/
		} else {
			text = "system not complete";
			canvas.drawText(text, m_xoffset, ty, m_headerPaint);
		}
		
		m_headerPaint.setTextSize(m_textSize);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	public void uiAction(int action, int id, int value) {
		if (id == ACTION_SYSTEM_BACK) {
			m_game.setScreen(new MainMenuScreen(m_game));
		} else {
			m_buttons.get(getLevel() - 1).setSelected(false);
			setLevel(id);
			m_buttons.get(getLevel() - 1).setSelected(true);
		}
	}

}
