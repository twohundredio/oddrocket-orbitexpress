package com.oddrocket.orbitexpress;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdView;
import com.oddrocket.orbitexpress.gfw.Audio;
import com.oddrocket.orbitexpress.gfw.BlankScreen;
import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.GameRenderView;
import com.oddrocket.orbitexpress.gfw.GameTouchInput;

public class MainActivity extends Activity {
    private GameManager m_manager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        m_manager = new GameManager(this);
        GameRenderView renderView = new GameRenderView(this, m_manager);
        m_manager.setRenderer(renderView);
        m_manager.setTouchInput(new GameTouchInput(m_manager));
        m_manager.setScreen(new BlankScreen(m_manager));
        m_manager.setPreferences(getPreferences(Context.MODE_PRIVATE));
        m_manager.loadSettings();
        m_manager.configureAds();
        m_manager.setAudio(new Audio(m_manager));

        RelativeLayout layout = new RelativeLayout(this);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        renderView.setLayoutParams(lp);
        layout.addView(renderView);

        if (m_manager.isAdEnabled()) {
            AdView adView = m_manager.getAdView();
            layout.addView(adView);
        }

        setContentView(layout);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        m_manager.onResume();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        m_manager.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        m_manager.onDestroy();
    }

}
