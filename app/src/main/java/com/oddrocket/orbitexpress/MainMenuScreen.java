package com.oddrocket.orbitexpress;

import java.util.Random;

import com.oddrocket.orbitexpress.gfw.Collision;
import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.ui.Button;
import com.oddrocket.orbitexpress.ui.ButtonRenderSound;

import android.graphics.Canvas;

public class MainMenuScreen extends MenuScreen {
    private final Rocket m_rocket;
    private boolean m_rocketVisable = false;
    private double m_rocketTime = 0;
    private static final int ROCKETTIME = 5;
    private final Random randomGenerator = new Random();
    private final ButtonRenderSound m_soundButtonRender;

    private static final int ACTION_PLAY = 1;
    private static final int ACTION_SOUND = 2;
    private static final int ACTION_HELP = 3;
    private static final int ACTION_HIGHSCORE = 4;

    public MainMenuScreen(GameManager game) {
        super(game);
        setTitle("orbit express");

        m_rocket = new Rocket(game);
        m_rocket.setSystemSize(300000, canvasWidth/300000.0);

        int buttonwidth = m_buttonWidth;
        int buttonheight = (int) (buttonwidth * 0.15);
        int buttonstarty = (int) (canvasHeight * 0.25);
        
        Button b = new Button(m_xoffset, buttonstarty, buttonwidth, buttonheight, ACTION_PLAY);
        b.setStyle(Button.STYLE_TEXT);
        b.setText("play");
        b.setTypeface(m_typeface);
        m_ui.addComponent(b);

        buttonstarty = (int) (buttonstarty + (1.5*buttonheight));

        b = new Button(m_xoffset, buttonstarty, buttonwidth, buttonheight, ACTION_HIGHSCORE);
        b.setStyle(Button.STYLE_TEXT);
        b.setText("highscore");
        b.setTypeface(m_typeface);
        m_ui.addComponent(b);

        buttonstarty = (int) (buttonstarty + (1.5*buttonheight));

        b = new Button(m_xoffset, buttonstarty, buttonwidth, buttonheight, ACTION_HELP);
        b.setStyle(Button.STYLE_TEXT);
        b.setText("help");
        b.setTypeface(m_typeface);
        m_ui.addComponent(b);

        int buttonSize = (m_textSize - m_xoffset) * 2;
        b = new Button(canvasWidth - (buttonSize + m_xoffset), m_xoffset, buttonSize, buttonSize, ACTION_SOUND);
        m_soundButtonRender = new ButtonRenderSound();
        b.setRender(m_soundButtonRender);
        m_ui.addComponent(b);
    }

    @Override
    public void update(float timeDelta) {
        // Sound
        m_soundButtonRender.setSoundEnabled(m_game.getSettings().soundEnabled());

        // Rocket
        if (m_rocketVisable) {
            if (!Collision.boxCollision(m_rocket.getX(), m_rocket.getY(),
                    m_rocket.getSize(), m_rocket.getSize(),
                    0, 0, canvasWidth, canvasHeight)) {
                m_rocketVisable = false;
                m_rocketTime = 0;
                m_rocket.launchPad();
            } else {
                m_rocket.update(timeDelta);
            }
        } else {
            m_rocketTime = m_rocketTime + timeDelta;
            if (m_rocketTime > ROCKETTIME) {
                float x;
                float y;
                float angle;

                switch (randomGenerator.nextInt(4)) {
                case 0:
                    x = 1;
                    y = 1 + randomGenerator.nextInt(canvasHeight-1);
                    angle = 45 + randomGenerator.nextInt(90);
                    break;
                case 1:
                    x = canvasWidth - 1;
                    y = 1 + randomGenerator.nextInt(canvasHeight-1);
                    angle = 225 + randomGenerator.nextInt(90);
                    break;
                case 2:
                    y = 1;
                    x = 1 + randomGenerator.nextInt(canvasWidth-1);
                    angle = 135 + randomGenerator.nextInt(90);
                    break;
                default:
                    y = canvasHeight - 1;
                    x = 1 + randomGenerator.nextInt(canvasWidth-1);
                    angle = 315 + randomGenerator.nextInt(90);
                    break;
                }

                m_rocket.setLaunchLocation(x, y, angle, 0);
                m_rocket.setX(x);
                m_rocket.setY(y);
                m_rocket.setPower(1 + randomGenerator.nextInt(25));
                m_rocket.launch();
                m_rocketVisable = true;
            }
        }
    }

    @Override
    public void present(Canvas canvas, float timeDelta) {
        super.present(canvas, timeDelta);
        if (m_rocketVisable) {
            m_rocket.doDraw(canvas, timeDelta);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        m_game.showAds();
    }

    @Override
    public void dispose() {
        m_game.hideAds();
    }

    @Override
    public void uiAction(int action, int id, int value) {
        switch (id) {
        case ACTION_PLAY:
            m_game.setScreen(new SelectSystemScreen(m_game));
            break;
        case ACTION_HELP:
            m_game.setScreen(new HelpScreen(m_game));
            break;
        case ACTION_HIGHSCORE:
            m_game.setScreen(new HighscoreScreen(m_game));
            break;
        case ACTION_SOUND:
            m_game.getSettings().setSoundEnabled(!m_game.getSettings().soundEnabled());
            m_game.saveSettings();
            break;
        }
    }

}
