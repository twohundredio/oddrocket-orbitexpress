package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.Screen;
import com.oddrocket.orbitexpress.ui.UiCallback;
import com.oddrocket.orbitexpress.ui.UiOverlay;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.Paint.Align;

public class MenuScreen implements Screen, UiCallback {
	final GameManager m_game;
	final UiOverlay m_ui = new UiOverlay();
	final int canvasWidth;
	final int canvasHeight;
	final int m_xoffset;
	final int m_textSize;
	final int m_textSizeHalf;
	private String m_text;
	final Typeface m_typeface;
	final int m_roundButtonSize;
	final int m_buttonWidth;
	final int m_buttonHeight;
	final Paint m_headerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Align m_headAlign = Align.LEFT;
	
	MenuScreen(GameManager game) {
		m_game = game;
		m_ui.setCallback(this);
		m_typeface = m_game.getTypeface(); 
		
		/* Set sizes */
		canvasWidth = m_game.getRenderer().getScreenWidth();
		canvasHeight = m_game.getRenderer().getScreenHeight();
		m_xoffset = (int) (canvasWidth * 0.02);
		m_textSize = (int) (canvasHeight * 0.1);
		m_textSizeHalf = m_textSize/2;
		m_roundButtonSize = (m_textSize - m_xoffset) * 2;
		m_buttonWidth = (int) (canvasWidth * 0.4);
		m_buttonHeight = (int) (m_buttonWidth * 0.15);
		
		/* Paint */
		m_headerPaint.setTypeface(m_typeface);
		m_headerPaint.setColor(Color.GREEN);
		m_headerPaint.setTextSize(m_textSize); 
	}
	
	void setTitle(String text) {
		m_text = text;
	}
	
	void setTitleAlign(Align align) {
		m_headAlign = align;
	}
	
	private void drawHeader(Canvas canvas) {
		int x = m_xoffset;
		if (m_headAlign == Align.CENTER) {
			x = canvasWidth / 2;
			m_headerPaint.setTextAlign(m_headAlign);
		}
		canvas.drawText(m_text, x, m_textSize, m_headerPaint);
	}

	@Override
	public void uiAction(int action, int id, int value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float timeDelta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void present(Canvas canvas, float timeDelta) {
		m_game.getBackground().doDraw(canvas);	
		drawHeader(canvas);
		m_ui.doDraw(canvas);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	public void clickDown(float x, float y) {
		m_ui.downHandle(x, y);
	}
	
	public void clickMove(float x, float y) {
		m_ui.moveHandle(x, y);
	}
	
	public void clickUp(float x, float y) {
		m_ui.upHandle(x, y);
	}
}
