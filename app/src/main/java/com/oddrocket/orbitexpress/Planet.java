package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.ParticleEffect;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class Planet {
    private final GameManager m_game;
    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint m_paint_source = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint m_paint_target = new Paint(Paint.ANTI_ALIAS_FLAG);
    private float centerX;
    private float centerY;
    private final double m_orbit;
    private double m_orbit_scale;
    private final double m_orbit_period;
    private double m_planet_scale;
    private final float drawradius;
    private double m_cx = 0;
    private double m_cy = 0;
    private boolean m_isTarget;
    private boolean m_isSource;
    private float m_orbitradius;
    private float m_planetradius;
    private float m_targetradius;
    private final int m_start_angle;
    private float m_time = 0;
    private final int m_render;
    private boolean m_clockwise;

    private final ParticleEffect m_particle;

    public Planet(GameManager game, double radius, double mass, double orbit, double orbitperiod, int startAngle, int colour, int render) {
        m_game = game;

        paint.setColor(colour);
        paint.setStyle(Style.FILL);

        m_paint_source.setARGB(255, 0, 255, 0);
        m_paint_source.setStyle(Style.STROKE);

        m_paint_target.setARGB(255, 255, 0, 0);
        m_paint_target.setStyle(Style.STROKE);

        m_orbit = orbit;
        m_orbit_period = orbitperiod;
        m_clockwise = (radius % 10) == 0;
        drawradius = (float)radius;
        m_start_angle = startAngle;

        m_orbitradius = (float) (m_orbit * m_orbit_scale);
        m_planetradius = (float) (drawradius * m_planet_scale);
        m_targetradius = m_planetradius * 2;

        m_particle = new ParticleEffect(m_game);

        m_render = render;
        if (m_orbit_period != 0) {
            loadTexture();
        }
    }

    public void reset() {
        m_time = 0;
        m_isTarget = false;
        m_isSource = false;
        m_particle.reset();
        update(0);

        if (m_orbit_period == 0) {
            m_particle.addSun(centerX, centerY, m_planetradius);
        }
    }

    public void dispose() {
        m_particle.reset();
    }

    public void setTarget()
    {
        m_isTarget = true;
    }

    public boolean isTarget() {
        return m_isTarget;
    }

    public boolean isSource() {
        return m_isSource;
    }

    public void setSource()
    {
        m_isSource = true;
    }

    public void setCenter(float x, float y) {
        centerX = x;
        centerY = y;

        if (m_orbit_period == 0) {
            m_particle.addSun(x, y, m_planetradius);
        }

        update(0);
    }

    public double getOrbit() {
        return m_orbit;
    }

    public void setOrbitScale(double scale) {
        m_orbit_scale = scale;
        m_planet_scale = scale;

        m_orbitradius = (float) (m_orbit * m_orbit_scale);
        m_planetradius = (float) (drawradius * m_planet_scale);
        m_targetradius = m_planetradius + (float)(m_game.getRenderer().getScreenHeight() * 0.015);
    }

    public void setPlanetScale(double scale) {
        m_planet_scale = scale;
        setOrbitScale(m_planet_scale);
    }

    public float getRadius() {
        return drawradius;
    }

    public float getScaledRadius() {
        return m_planetradius;
    }

    public void update(float deltaTime) {
        int sx = getX();
        int sy = getY();
        m_time = m_time + deltaTime;
        if (m_time > m_orbit_period) {
            m_time = m_time - (float)m_orbit_period;
            if (m_time < 0) {
                m_time = 0;
            }
        }

        if (m_orbit_period == 0) {
            m_cx = 0;
            m_cy = 0;
        } else {
            double angdeg;
            if (m_clockwise) {
                angdeg = m_start_angle + ((m_time / m_orbit_period) * 360);
            } else {
                angdeg = m_start_angle - ((m_time / m_orbit_period) * 360);
            }
            m_cx = m_orbitradius * Math.cos(Math.toRadians(angdeg));
            m_cy = m_orbitradius * Math.sin(Math.toRadians(angdeg));
        }

        if (deltaTime > 0) {
            if (m_orbit_period != 0) {
                if (m_clockwise) {
                    m_particle.addTrail(centerX, centerY, sx, sy, getX(), getY(), 0.4f, m_orbitradius, m_planetradius);
                } else {
                    m_particle.addTrail(centerX, centerY, getX(), getY(), sx, sy, 0.4f, m_orbitradius, m_planetradius);
                }
            }
            m_particle.update(deltaTime);
        }
    }

    public int getX() {
        return (int) (centerX + m_cx);
    }

    public int getY() {
        return (int) (centerY + m_cy);
    }

    void doDraw(Canvas canvas) {
        int x;
        int y;

        x = getX();
        y = getY();

        if (m_orbit_period != 0) {
            m_particle.present(canvas, 0);
        }

        if (m_isTarget) {
            m_paint_target.setColor(Color.rgb(255, 0, 0));
            canvas.drawCircle(x, y, m_targetradius, m_paint_target);
            canvas.drawCircle(x, y, m_targetradius + 1, m_paint_target);
            m_paint_target.setColor(Color.rgb(255, 51, 51));
            canvas.drawCircle(x, y, m_targetradius + 2, m_paint_target);
            if (m_targetradius - m_planetradius >= 10) {
                canvas.drawCircle(x, y, m_targetradius + 3, m_paint_target);
                m_paint_target.setColor(Color.rgb(255, 102, 102));
                canvas.drawCircle(x, y, m_targetradius + 4, m_paint_target);
            }

        }
        if (m_isSource) {
            m_paint_source.setColor(Color.rgb(0, 255, 0));
            canvas.drawCircle(x, y, m_targetradius, m_paint_source);
            canvas.drawCircle(x, y, m_targetradius + 1, m_paint_source);
            m_paint_source.setColor(Color.rgb(51, 255, 51));
            canvas.drawCircle(x, y, m_targetradius + 2, m_paint_source);
            if (m_targetradius - m_planetradius >= 10) {
                canvas.drawCircle(x, y, m_targetradius + 3, m_paint_source);
                m_paint_source.setColor(Color.rgb(102, 255, 102));
                canvas.drawCircle(x, y, m_targetradius + 4, m_paint_source);
            }
        }

        if (m_orbit_period == 0) {
            m_particle.present(canvas, 0);
        }
        canvas.drawCircle(x, y, m_planetradius, paint);
    }

    private void loadTexture() {
        paint.setShader(m_game.getTextures().getBitmapShader(m_render));
    }
}
