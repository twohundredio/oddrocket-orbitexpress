package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.ParticleEffect;
import com.oddrocket.orbitexpress.util.AssetRead;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

class Rocket {
	private float x = 0;
	private float y = 0;
	private double angle = 0;
	private boolean m_launched = false;
	private final GameManager m_game;
	private Bitmap m_rocketbmp;
	private int width;
	private int height;
	private int blast_offset;
	private final Matrix matrix = new Matrix();
	private double m_systemRadius;
	private double m_systemScale;
	private double m_time;
	private double m_distance;
	private int m_power;
	private final ParticleEffect m_particle;
	private float last_blastx = 0;
	private float last_blasty = 0;
	private int blast_size;

	private static final int SPEED_SLOW = 10;
	private static final int SPEED_FAST = 2;
	private static final int BLAST_LOC = 92;
	private static final int BLAST_SIZE = 28;
	
	public Rocket(GameManager game) {
		m_game = game;
		m_particle = new ParticleEffect(game);
	}
	
	public void setSystemSize(double radius, double scale) {
		m_systemRadius = radius;
		m_systemScale = scale;
		
		m_rocketbmp = AssetRead.getBitmapFromAsset(m_game.getContext(), "rocket.png");
		height = (int) (25000 * m_systemScale);
		float rscale = (float)height/m_rocketbmp.getHeight();
		width = (int) (m_rocketbmp.getWidth() * rscale);
		blast_offset = (int) (BLAST_LOC * rscale);
		blast_size = (int) (BLAST_SIZE * rscale);
		m_rocketbmp = Bitmap.createScaledBitmap(m_rocketbmp, width, height, false);
	}
	
	public void setPower(int power) {
		m_power = power;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setLaunchLocation(float x, float y, float angle, float planetRadius) {
		if (!m_launched) {
			double a = Math.toRadians(angle - 90);
			float hwidth = width / 2;
			float hheight = height / 2;
			float mx = (x + ((planetRadius + (height/2)) * (float)Math.cos(a)));
			float my = (y + ((planetRadius + (height/2)) * (float)Math.sin(a)));
			setAngle(angle);
			setX(mx - hwidth);
			setY(my - hheight);
		}
	}

	private void setAngle(double angle) {
		this.angle = angle;
	}
	
	public float getCenterX() {
		return x + (width/2);
	}
	
	public float getCenterY() {
		return y + (height/2);
	}
	
	public int getSize() {
		return (width > height) ? width : height;
	}
	
	public void update(float deltaTime) {
		if (m_launched) {
			if (deltaTime != 0) {
				m_time = m_time + deltaTime;
				int s = (int) (SPEED_SLOW - ((SPEED_SLOW - SPEED_FAST) * (((double)m_power)/100)));
				double d = ((m_systemRadius * m_systemScale) * deltaTime) / s;
				x = (float) (x + d * Math.cos(Math.toRadians(angle - 90)));
				y = (float) (y + d * Math.sin(Math.toRadians(angle - 90)));
				m_distance = m_distance + (d/m_systemScale);
				
				m_particle.update(deltaTime);
				
				float bx = getCenterX();
				float by = getCenterY();
				bx -= blast_offset * Math.cos(Math.toRadians(angle - 90));
				by -= blast_offset * Math.sin(Math.toRadians(angle - 90));
				if (last_blastx !=0 && last_blasty != 0) {
					m_particle.addRocket(bx, by, last_blastx, last_blasty, blast_size, 0.5f);
				}
				last_blastx = bx;
				last_blasty = by;
			}
		}
	}
	
	void doDraw(Canvas canvas, float deltaTime) {
		matrix.setRotate((float) angle, getCenterX(), getCenterY());
		
		m_particle.present(canvas, deltaTime);
		
		canvas.setMatrix(matrix);
		canvas.drawBitmap(m_rocketbmp, x, y, null);
		canvas.setMatrix(null);
	}
	
	public void launch() {
		m_launched = true;
		m_time = 0;
		m_distance = 0;
	}
	
	public void launchPad() {
		m_launched = false;
		m_particle.reset();
		last_blastx = last_blasty = 0;
	}
	
	public void dispose() {
		m_particle.reset();
	}
	
	public boolean isLaunched() {
		return m_launched;
	}
	
	public double getFlightDistance() {
		return m_distance;
	}
	
	public double getFlightTime() {
		return m_time;
	}
}
