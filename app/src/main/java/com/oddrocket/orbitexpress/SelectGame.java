package com.oddrocket.orbitexpress;

class SelectGame {
	private int system;
	private int mission;
	
	int getSystem() {
		return system;
	}
	void setSystem(int system) {
		this.system = system;
	}
	int getMission() {
		return mission;
	}
	void setMission(int mission) {
		this.mission = mission;
	}
}
