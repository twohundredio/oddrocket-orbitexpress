package com.oddrocket.orbitexpress;

import java.util.Vector;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.ui.Button;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class SelectMissionScreen extends MenuScreen {
	private final Vector<Rect> lines = new Vector<>();
	private final Paint m_greenpaint;
	private final Paint m_graypaint;
	private final SelectGame m_gs;
	
	private static final int ACTION_1 = 1;
	private static final int ACTION_2 = 2;
	private static final int ACTION_3 = 3;
	private static final int ACTION_4 = 4;
	private static final int ACTION_5 = 5;
	
	SelectMissionScreen(GameManager game, SelectGame gs) {
		super(game);
		setTitle("select mission");
		
		m_gs = gs;
		
		m_greenpaint = new Paint();
		m_greenpaint.setColor(Color.GREEN);
		
		m_graypaint = new Paint();
		m_graypaint.setColor(Color.DKGRAY);
		
		int sc = m_game.getHighscores().getStageComplete(m_gs.getSystem());
		
		int buttonwidth = (int) (canvasWidth * 0.1);
		int buttonstarty = (canvasHeight/2) - (buttonwidth /2);
		int buttonstartx = (canvasWidth/5) - (buttonwidth/2);
		int xinc = buttonstartx;
		
		Rect r = new Rect();
		r.top = r.bottom = canvasHeight/2;
		r.left = 0;
		r.right = buttonstartx;
		lines.add(r);
		
		Button b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_1);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("1");
		b.setTypeface(m_typeface);
		m_ui.addComponent(b);
		
		r = new Rect();
		r.top = r.bottom = canvasHeight/2;
		r.left = buttonstartx + buttonwidth + 1;
		buttonstartx = buttonstartx + xinc;
		r.right = buttonstartx;
		lines.add(r);
		
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_2);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("2");
		b.setTypeface(m_typeface);
		if (sc < 1) {
			b.setInactive();
		}
		m_ui.addComponent(b);
		
		r = new Rect();
		r.top = r.bottom = canvasHeight/2;
		r.left = buttonstartx + buttonwidth + 1;
		buttonstartx = buttonstartx + xinc;
		r.right = buttonstartx;
		lines.add(r);
		
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_3);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("3");
		b.setTypeface(m_typeface);
		if (sc < 2) {
			b.setInactive();
		}
		m_ui.addComponent(b);
		
		r = new Rect();
		r.top = r.bottom = canvasHeight/2;
		r.left = buttonstartx + buttonwidth + 1;
		buttonstartx = buttonstartx + xinc;
		r.right = buttonstartx;
		lines.add(r);
		
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_4);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("4");
		b.setTypeface(m_typeface);
		if (sc < 3) {
			b.setInactive();
		}
		m_ui.addComponent(b);
		
		r = new Rect();
		r.top = r.bottom = canvasHeight/2;
		r.left = buttonstartx + buttonwidth + 1;
		buttonstartx = buttonstartx + xinc;
		r.right = buttonstartx;
		lines.add(r);
		
		b = new Button(buttonstartx, buttonstarty, buttonwidth, buttonwidth, ACTION_5);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("5");
		b.setTypeface(m_typeface);
		if (sc < 4) {
			b.setInactive();
		}
		m_ui.addComponent(b);
		
		r = new Rect();
		r.top = r.bottom = canvasHeight/2;
		r.left = buttonstartx + buttonwidth + 1;
		r.right = canvasWidth;
		lines.add(r);
	}

	@Override
	public void update(float timeDelta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void present(Canvas canvas, float timeDelta) {
		super.present(canvas, timeDelta);
		
		int sc = m_game.getHighscores().getStageComplete(m_gs.getSystem());
		sc = sc + 1;
		for (Rect r : lines) {
			Paint p = m_graypaint;
			if (sc > 0) {
				sc--;
				p = m_greenpaint;
			}
			canvas.drawLine(r.left, r.top, r.right, r.bottom, p);
		}
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void uiAction(int action, int id, int value) {
		m_gs.setMission(id);
		m_game.setScreen(new SolarSystemScreen(m_game, m_gs));
	}
}