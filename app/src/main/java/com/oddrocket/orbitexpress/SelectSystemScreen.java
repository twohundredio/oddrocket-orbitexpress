package com.oddrocket.orbitexpress;

import java.util.Vector;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.ui.Button;
import com.oddrocket.orbitexpress.ui.ButtonRenderBack;
import com.oddrocket.orbitexpress.ui.ButtonRenderNext;

import android.graphics.Canvas;
import android.graphics.Paint.Align;

public class SelectSystemScreen extends MenuScreen {
	private int m_screen = 0;
	
	private final int m_system_text_x1;
	private final int m_system_text_x2;
	private final int m_system_text_y;
	
	private final int m_system_x1;
	private final int m_system_y1;
	private final int m_system_x2;
	private final int m_system_y2;
	
	private final Button m_button_back;
	private final Button m_button_next;
	
	private final Vector<SolarSystem> m_systems = new Vector<>();
	
	private static final int ACTION_SYSTEM_1 = 1;
	private static final int ACTION_SYSTEM_2 = 2;
	private static final int ACTION_LEFT = 3;
	private static final int ACTION_RIGHT = 4;
	
	SelectSystemScreen(GameManager game) {
		super(game);
		setTitle("select system");
		
		int buttonsize = m_roundButtonSize;
		int posy = (canvasHeight/2) - (buttonsize/2);
		
		m_button_back = new Button(m_xoffset, posy, buttonsize, buttonsize, ACTION_LEFT);
		m_button_back.setRender(new ButtonRenderBack());
		m_button_back.setInactive();
		m_ui.addComponent(m_button_back);
		
		m_button_next = new Button(canvasWidth - (m_xoffset + buttonsize), posy, buttonsize, buttonsize, ACTION_RIGHT);
		m_button_next.setRender(new ButtonRenderNext());
		m_ui.addComponent(m_button_next);
		
		Button b;
		int bssize = (m_xoffset * 8) + (buttonsize * 2);
		int bsy = (canvasHeight/2) - (bssize / 2);
		
		int bx = (m_xoffset * 3) + buttonsize;
		m_system_text_x1 = bx + (bssize / 2);
		m_system_x1 = bx + (bssize/2);
		m_system_y1 = bsy + (bssize/2);
		b = new Button(bx, bsy, bssize, bssize, ACTION_SYSTEM_1);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("");
		m_ui.addComponent(b);
		
		bx = canvasWidth - ((m_xoffset * 3) + buttonsize + bssize);
		m_system_text_x2 = bx + (bssize / 2);
		m_system_x2 = bx + (bssize/2);
		m_system_y2 = bsy + (bssize/2);
		b = new Button(bx, bsy, bssize, bssize, ACTION_SYSTEM_2);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("");
		m_ui.addComponent(b);
		
		m_system_text_y = bsy + bssize + m_textSize;
		
		loadSystems((int) (bssize * 0.8));
	}
	
	private void loadSystems(int size) {
		SolarSystem system = new SolarSystem(m_game);
		system.load("system1.txt");
		system.createScale(size, size);
		system.setCenter(m_system_x1, m_system_y1);
		m_systems.add(system);
		
		system = new SolarSystem(m_game);
		system.load("system2.txt");
		system.createScale(size, size);
		system.setCenter(m_system_x2, m_system_y2);
		m_systems.add(system);
		
		system = new SolarSystem(m_game);
		system.load("system3.txt");
		system.createScale(size, size);
		system.setCenter(m_system_x1, m_system_y1);
		m_systems.add(system);
		
		system = new SolarSystem(m_game);
		system.load("system4.txt");
		system.createScale(size, size);
		system.setCenter(m_system_x2, m_system_y2);
		m_systems.add(system);
	}

	@Override
	public void update(float timeDelta) {
		m_systems.get((m_screen * 2)).update(timeDelta);
		m_systems.get(1 + (m_screen * 2)).update(timeDelta);
	}

	@Override
	public void present(Canvas canvas, float timeDelta) {
		super.present(canvas, timeDelta);
		
		m_systems.get((m_screen * 2)).doDraw(canvas, timeDelta);
		m_systems.get(1 + (m_screen * 2)).doDraw(canvas, timeDelta);

		m_headerPaint.setTextAlign(Align.CENTER);
		if (m_screen == 0) {
			canvas.drawText("one", m_system_text_x1, m_system_text_y, m_headerPaint);
			canvas.drawText("two", m_system_text_x2, m_system_text_y, m_headerPaint);
		} else {
			canvas.drawText("three", m_system_text_x1, m_system_text_y, m_headerPaint);
			canvas.drawText("four", m_system_text_x2, m_system_text_y, m_headerPaint);
		}
		m_headerPaint.setTextAlign(Align.LEFT);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		for (SolarSystem s : m_systems) {
			s.dispose();
		}
	}

	public void uiAction(int action, int id, int value) {
		if (id == ACTION_LEFT) {
			m_screen = 0;
			m_button_back.setInactive();
			m_button_next.setActive();
		}
		if (id == ACTION_RIGHT) {
			m_screen = 1;
			m_button_back.setActive();
			m_button_next.setInactive();
		}
		if (id == ACTION_SYSTEM_1 || id == ACTION_SYSTEM_2) {
			SelectGame gs = new SelectGame();
			gs.setSystem(((id == ACTION_SYSTEM_1) ? 1 : 2) + (m_screen * 2));
			m_game.setScreen(new SelectMissionScreen(m_game,gs));
		}
	}

}
