package com.oddrocket.orbitexpress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import com.oddrocket.orbitexpress.gfw.GameManager;

import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;

class SolarSystem {
    private final Vector<Planet> m_planets = new Vector<>();
    private Planet m_source;
    private float m_centerx;
    private float m_centery;
    private double m_radiusPx;
    private final GameManager m_game;
    private double m_scale;
    private double m_systemRadius;

    class Mission {
        public int start;
        public int end;
    }

    private final Vector<Mission> m_missions = new Vector<>();

    SolarSystem(GameManager game) {
        m_game = game;
    }

    public void reset() {
        for (Planet planet : m_planets) {
            planet.reset();
        }
    }

    public void dispose() {
        for (Planet planet : m_planets) {
            planet.dispose();
        }
    }
    
    private void addPlanet(Planet planet)
    {
        m_planets.add(planet);
    }

    private Planet getPlanetAt(int index)
    {
        return m_planets.elementAt(index);
    }

    public Vector<Planet> getPlanets() {
        return m_planets;
    }

    public void setMission(int mission) {
        Mission m = m_missions.elementAt(mission - 1);
        getPlanetAt(m.start).setSource();
        getPlanetAt(m.end).setTarget();
        m_source = getPlanetAt(m.start);
    }

    public Planet getSourcePlanet() {
        return m_source;
    }

    double getRadiusPx()
    {
        return m_radiusPx;
    }

    double getScale() {
        return m_scale;
    }

    double getSystemRadius() {
        return m_systemRadius;
    }

    void setCenter(float x, float y)
    {
        m_centerx = x;
        m_centery = y;
        for (Planet planet : m_planets) {
            planet.setCenter(m_centerx, m_centery);
        }
    }

    float getCenterX()
    {
        return m_centerx;
    }

    float getCenterY()
    {
        return m_centery;
    }

    void createScale(float width, float height)
    {
        float min = (width < height) ? width : height;
        double largestorbit = m_planets.get(0).getOrbit();
        double orbitscale;

        for (Planet planet : m_planets) {
            if (planet.getOrbit() > largestorbit) {
                largestorbit = planet.getOrbit();
            }
        }

        orbitscale = ((min/2)*0.9) / largestorbit;
        for (Planet planet : m_planets) {
            planet.setOrbitScale(orbitscale);
        }

        m_radiusPx = largestorbit * orbitscale;
        m_scale = orbitscale;
        m_systemRadius = largestorbit;
    }

    void update(float timeDelta) {
        for (Planet planet : m_planets) {
            planet.update(timeDelta);
        }
    }

    void doDraw(Canvas canvas, float timeDelta)
    {
        for (Planet planet : m_planets) {
            planet.doDraw(canvas);
        }
    }

    void load(String asset) {
        AssetManager assetManager = m_game.getContext().getAssets();
        InputStream istr;
        BufferedReader reader = null;
        int planets = 0;
        boolean planetsInput = false;
        int missions = 0;

        try {
            istr = assetManager.open(asset);
            reader = new BufferedReader(new InputStreamReader(istr));

            String mLine = reader.readLine();
            while (mLine != null) {
               //process line
               if (!mLine.startsWith("#")) {
                   if (!planetsInput) {
                       if (planets == 0) {
                           planets = Integer.parseInt(mLine);
                       } else {
                           loadPlanet(mLine);
                           planets--;
                           if (planets == 0) {
                               planetsInput = true;
                           }
                       }
                   } else {
                       // Load missions
                       if (missions == 0) {
                           missions = Integer.parseInt(mLine);
                       } else {
                           readMisison(mLine);
                       }
                   }
               }
               mLine = reader.readLine();
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                 try {
                     reader.close();
                 } catch (IOException e) {
                     //log the exception
                 }
            }
        }
    }

    private void readMisison(String line) {
        String[] tokensVal;
        Mission mission = new Mission();

        tokensVal = line.split(",");
        mission.start = Integer.parseInt(tokensVal[0]);
        mission.end = Integer.parseInt(tokensVal[1]);

        m_missions.add(mission);
    }

    private void loadPlanet(String line) {
        String[] tokensVal;
        Planet p;
        double radius;
        double mass;
        double orbit;
        double orbitperiod;
        int start_angle;
        int r;
        int g;
        int b;
        int render;

        tokensVal = line.split(",");
        radius = Double.parseDouble(tokensVal[0]);
        mass = Double.parseDouble(tokensVal[1]);
        orbit = Double.parseDouble(tokensVal[2]);
        orbitperiod = Double.parseDouble(tokensVal[3]);
        start_angle = Integer.parseInt(tokensVal[4]);
        r = Integer.parseInt(tokensVal[5]);
        g = Integer.parseInt(tokensVal[6]);
        b = Integer.parseInt(tokensVal[7]);
        render = Integer.parseInt(tokensVal[8]);

        p = new Planet(m_game, radius, mass, orbit, orbitperiod, start_angle, Color.rgb(r, g, b), render);
        addPlanet(p);
    }
}
