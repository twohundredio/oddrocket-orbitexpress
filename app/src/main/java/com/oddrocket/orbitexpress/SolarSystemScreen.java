package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.Audio;
import com.oddrocket.orbitexpress.gfw.Collision;
import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.ParticleEffect;
import com.oddrocket.orbitexpress.gfw.Screen;
import com.oddrocket.orbitexpress.ui.Button;
import com.oddrocket.orbitexpress.ui.ButtonRenderPause;
import com.oddrocket.orbitexpress.ui.LevelSelector;
import com.oddrocket.orbitexpress.ui.RotarySelector;
import com.oddrocket.orbitexpress.ui.UiCallback;
import com.oddrocket.orbitexpress.ui.UiOverlay;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;

public class SolarSystemScreen implements Screen, UiCallback  {
    private final GameManager m_game;
    private final SolarSystem m_system;
    private final Rocket m_rocket;
    private final UiOverlay m_ui = new UiOverlay();
    private final LevelSelector m_power_selector;
    private final RotarySelector m_angle_selector;
    private final ParticleEffect m_particles;

    private static final int ACTION_IGNORE = 1;
    private static final int ACTION_LAUNCH = 99;
    private static final int ACTION_PAUSE = 88;
    private static final int ACTION_MENU = 89;
    private static final int ACTION_PAUSECONTINUE = 90;
    private static final int ACTION_RETRY = 91;
    private static final int ACTION_CONTINUE = 92;

    private static final int DELAY_TIME = 2;

    private int canvasWidth = 200;
    private int canvasHeight = 400;
    private final int m_xoffset;
    private final int m_textSize;
    private final int m_textSizeHalf;
    private int m_mission = 1;
    private int m_level = 1;
    private double m_delayTimer = 0;
    private boolean m_ignoreSourceCrash = true;

    private UiOverlay m_pauseui = null;
    private final Button m_pauseButton;
    private final Paint m_blackPaint = new Paint();
    private final Paint m_greenPaint = new Paint();
    private final Paint textPaint;
    private final int uicsize;
    private final int uicalign;
    private final int m_cy_power;
    private final int m_cy_angle;
    private final int m_cy_launch;

    enum State { PAUSE_START, PAUSE_LAUNCH,
        REQUEST_PAUSE, REQUEST_PAUSE_CRASH, REQUEST_PAUSE_TARGET,
        REQUEST_PAUSE_START, REQUEST_PAUSE_LAUNCH,
        REQUEST_CONTINUE_START, REQUEST_CONTINUE_LAUNCH,
        REQUEST_CONTINUE, REQUEST_RETRY, START,
        LAUNCH, CRASH_DELAY, CRASH, TARGET_DELAY, TARGET, LOST }
    private State m_state = State.START;

    SolarSystemScreen(GameManager game, SelectGame gs)
    {
        m_game = game;

        canvasWidth = m_game.getRenderer().getScreenWidth();
        canvasHeight = m_game.getRenderer().getScreenHeight();

        m_blackPaint.setColor(Color.BLACK);
        m_blackPaint.setStyle(Style.FILL);
        m_greenPaint.setColor(Color.GREEN);
        m_greenPaint.setStyle(Style.FILL);

        m_particles = new ParticleEffect(game);

        m_xoffset = (int) (canvasWidth * 0.02);
        m_textSize = (int) (canvasHeight * 0.1);
        m_textSizeHalf = m_textSize/2;

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setTypeface(m_game.getTypeface());
        textPaint.setColor(Color.GREEN);
        textPaint.setTextSize(m_textSize);
        textPaint.setTextAlign(Align.CENTER);

        double system_width;
        double ui_width;

        m_system = new SolarSystem(m_game);
        String systemname = "system1.txt";
        switch (gs.getSystem()) {
        case 1:
            systemname = "system1.txt";
            break;
        case 2:
            systemname = "system2.txt";
            break;
        case 3:
            systemname = "system3.txt";
            break;
        case 4:
            systemname = "system4.txt";
            break;
        }
        m_level = gs.getSystem();
        m_mission = gs.getMission();
        m_system.load(systemname);
        m_system.setMission(gs.getMission());
        m_system.createScale(canvasWidth, canvasHeight);
        system_width = (canvasWidth * 0.05) +  m_system.getRadiusPx();

        /* UI*/
        ui_width = canvasWidth - (system_width * 2);
        uicsize = (int) (ui_width * 0.3);
        //uicalign = (int) ((system_width * 2) + (ui_width/2) - (uicsize / 2));
        uicalign = (int) (canvasWidth - uicsize - (canvasWidth * 0.1));
        int verticalpad = (int) (canvasHeight * 0.1);

        int sx = (int) (((canvasWidth - (canvasWidth - uicalign))/2) - (canvasWidth * 0.01));
        m_system.setCenter(sx , canvasHeight/2);

        m_rocket = new Rocket(m_game);
        m_rocket.setX(0);
        m_rocket.setY(0);
        m_rocket.setSystemSize(m_system.getSystemRadius(), m_system.getScale());

        m_ui.setCallback(this);
        m_cy_power = verticalpad;
        m_cy_angle = (canvasHeight/2) - (uicsize/2);
        m_cy_launch = canvasHeight - uicsize - verticalpad;
        m_power_selector = new LevelSelector(uicalign, m_cy_power, uicsize, uicsize, ACTION_IGNORE);
        m_angle_selector = new RotarySelector(uicalign, m_cy_angle, uicsize, uicsize, ACTION_IGNORE);
        m_ui.addComponent(m_power_selector);
        m_ui.addComponent(m_angle_selector);
        m_ui.addComponent(new Button(uicalign, m_cy_launch, uicsize, uicsize, ACTION_LAUNCH));

        int smallButtonSize = (m_textSize - m_xoffset) * 2;
        m_pauseButton = new Button(m_xoffset, m_xoffset, smallButtonSize, smallButtonSize, ACTION_PAUSE);
        m_pauseButton.setRender(new ButtonRenderPause());
        m_ui.addComponent(m_pauseButton);
    }

    private synchronized void setState(State state) {
        if (state == State.REQUEST_PAUSE) {
            if (m_state == State.START) {
                m_state = State.REQUEST_PAUSE_START;
            } else if (m_state == State.LAUNCH) {
                m_state = State.REQUEST_PAUSE_LAUNCH;
            } else if (m_state == State.CRASH_DELAY) {
                m_state = State.REQUEST_PAUSE_CRASH;
            } else if (m_state == State.TARGET_DELAY) {
                m_state = State.REQUEST_PAUSE_TARGET;
            }
        } else if (state == State.REQUEST_CONTINUE) {
            if (m_state == State.PAUSE_LAUNCH) {
                m_state = State.REQUEST_CONTINUE_LAUNCH;
            } else if (m_state == State.PAUSE_START) {
                m_state = State.REQUEST_CONTINUE_START;
            }
        } else {
            m_state = state;
        }
    }

    private synchronized State getState() {
        return m_state;
    }

    @Override
    public void update(float timeDelta) {
        State state = getState();

        m_particles.update(timeDelta);

        /* Handle delay */
        if (state == State.REQUEST_RETRY) {
            m_pauseui = null;
            setState(State.START);
            m_rocket.launchPad();
            m_system.reset();
            m_angle_selector.reset();
            m_power_selector.reset();
            m_system.setMission(m_mission);
            m_delayTimer = 0;
            m_ui.setAllActive();
            m_particles.reset();
            m_game.getAudio().stopAll();
        }

        /* Handle Continue */
        if (state == State.REQUEST_CONTINUE_START) {
            m_pauseui = null;
            m_ui.setAllActive();
            setState(State.START);
            m_game.getAudio().resume();
        }
        if (state == State.REQUEST_CONTINUE_LAUNCH) {
            m_pauseui = null;
            m_pauseButton.setActive();
            setState(State.LAUNCH);
            m_game.getAudio().resume();
        }

        /* Handle Pause */
        if (state == State.REQUEST_PAUSE_START) {
            setState(State.PAUSE_START);
            m_ui.setAllInactive();
            createPauseUi();
            m_game.getAudio().pause();
        }
        if (state == State.REQUEST_PAUSE_LAUNCH) {
            setState(State.PAUSE_LAUNCH);
            m_ui.setAllInactive();
            createPauseUi();
            m_game.getAudio().pause();
        }

        /* Handle Crash and Delay */
        if (state == State.CRASH_DELAY) {
            m_delayTimer = m_delayTimer + timeDelta;
            if (m_delayTimer > DELAY_TIME) {
                state = State.REQUEST_PAUSE_CRASH;
            }
        }
        if (state == State.REQUEST_PAUSE_CRASH) {
            setState(State.CRASH);
            m_ui.setAllInactive();
            createPauseUi();
        }
        if (state == State.TARGET_DELAY) {
            m_delayTimer = m_delayTimer + timeDelta;
            if (m_delayTimer > 0) {
                state = State.REQUEST_PAUSE_TARGET;
            }
        }
        if (state == State.REQUEST_PAUSE_TARGET) {
            setState(State.TARGET);
            m_ui.setAllInactive();
            createPauseUi();
        }

        /* Handle Start and Launch */
        if (state == State.START || state == State.LAUNCH) {
            m_system.update(timeDelta);
            m_rocket.update(timeDelta);

            if (state == State.START) {
                m_rocket.setLaunchLocation(m_system.getSourcePlanet().getX(),
                        m_system.getSourcePlanet().getY(),
                        m_angle_selector.getAngle(),
                        m_system.getSourcePlanet().getScaledRadius());
            } else {
                if (!m_rocket.isLaunched()) {
                    m_ignoreSourceCrash = true;
                    m_rocket.setPower(m_power_selector.getLevel());
                    m_rocket.launch();
                    m_ui.setAllInactive();
                    m_pauseButton.setActive();
                    m_game.getAudio().playSound(Audio.Sounds.LAUNCH);
                }
            }

            if (m_rocket.isLaunched()) {
                if (!Collision.boxCollision(m_rocket.getX(), m_rocket.getY(),
                        m_rocket.getSize(), m_rocket.getSize(),
                        0, 0, canvasWidth, canvasHeight)) {
                    setState(State.LOST);
                    m_ui.setAllInactive();
                    createPauseUi();
                    m_game.getAudio().stopAll();
                }

                for (Planet p : m_system.getPlanets()) {
                    boolean collide = Collision.circleCollision(
                            m_rocket.getCenterX(), m_rocket.getCenterY(), m_rocket.getSize()/2,
                            p.getX(), p.getY(), p.getScaledRadius());
                    if (collide) {
                        if (p.isTarget()) {
                            setState(State.TARGET_DELAY);
                            m_game.getAudio().playSound(Audio.Sounds.LEVEL_COMPLETE);
                            m_game.getHighscores().setScore(m_level, m_mission,
                                    (int)m_rocket.getFlightDistance(), (int)m_rocket.getFlightTime());
                            m_game.saveSettings();
                        } else {
                            if (!(p.isSource() && m_ignoreSourceCrash)) {
                                setState(State.CRASH_DELAY);
                                m_game.getAudio().playSound(Audio.Sounds.EXPLOSION);
                                m_particles.addExplosion(m_rocket.getCenterX(), m_rocket.getCenterY(),
                                        m_rocket.getHeight()/2, DELAY_TIME);
                            }
                        }
                        break;
                    }

                    if (p.isSource() && m_ignoreSourceCrash) {
                        /* Check 2x radius to make sure rocket clears */
                        collide = Collision.circleCollision(
                                m_rocket.getCenterX(), m_rocket.getCenterY(), m_rocket.getSize()/2,
                                p.getX(), p.getY(), p.getScaledRadius() * 2);

                        if (!collide) {
                            m_ignoreSourceCrash = false;
                        }
                    }
                }
            }
        }
    }

    private void createPauseUi() {
        m_pauseui = new UiOverlay();
        m_pauseui.setCallback(this);

        int buttonwidth = (int) (canvasWidth * 0.4);
        int buttonheight = (int) (buttonwidth * 0.15);
        int buttonstarty = (int) (canvasHeight * 0.4);
        int buttonx = (canvasWidth / 2) - (buttonwidth / 2);

        Button b;

        if (m_state != State.TARGET) {
            b = new Button(buttonx, buttonstarty, buttonwidth, buttonheight, ACTION_RETRY);
            b.setStyle(Button.STYLE_TEXT);
            b.setText("restart");
            b.setTypeface(m_game.getTypeface());
            m_pauseui.addComponent(b);

            buttonstarty = (int) (buttonstarty + (1.5*buttonheight));
        }
        if (m_state == State.PAUSE_START || m_state == State.PAUSE_LAUNCH ||
                m_state == State.TARGET) {
            int action = ACTION_PAUSECONTINUE;
            if (m_state == State.TARGET) {
                action = ACTION_CONTINUE;
                buttonstarty = buttonstarty + buttonheight;
            }
            b = new Button(buttonx, buttonstarty, buttonwidth, buttonheight, action);
            b.setStyle(Button.STYLE_TEXT);
            b.setText("continue");
            b.setTypeface(m_game.getTypeface());
            m_pauseui.addComponent(b);

            buttonstarty = (int) (buttonstarty + (1.5*buttonheight));
        }

        b = new Button(buttonx, buttonstarty, buttonwidth, buttonheight, ACTION_MENU);
        b.setStyle(Button.STYLE_TEXT);
        b.setText("main menu");
        b.setTypeface(m_game.getTypeface());
        m_pauseui.addComponent(b);
    }

    private void drawPause(Canvas canvas) {
        int hwidth = canvasWidth/2;
        int uwidth = (int) (canvasWidth * 0.2);
        int bwidth = canvasWidth - (uwidth * 2);
        int textSize = (int) (canvasHeight * 0.1);
        canvas.drawRect(uwidth, 0, uwidth + bwidth, canvasHeight, m_blackPaint);
        canvas.drawLine(uwidth, 0, uwidth, canvasHeight, m_greenPaint);
        canvas.drawLine(uwidth + bwidth, 0, uwidth + bwidth, canvasHeight, m_greenPaint);

        if (m_state == State.PAUSE_START || m_state == State.PAUSE_LAUNCH) {
            canvas.drawText("paused", hwidth, textSize, textPaint);
        }
        if (m_state == State.LOST) {
            canvas.drawText("mission failure", hwidth, textSize, textPaint);
            canvas.drawText("rocket lost", hwidth, textSize + textSize, textPaint);
        }
        if (m_state == State.CRASH) {
            canvas.drawText("mission failure", hwidth, textSize, textPaint);
            canvas.drawText("rocket crashed", hwidth, textSize + textSize, textPaint);
        }
        if (m_state == State.TARGET) {
            canvas.drawText("mission", hwidth, textSize, textPaint);
            canvas.drawText("success", hwidth, textSize + textSize, textPaint);

            textPaint.setTextSize(m_textSizeHalf);
            canvas.drawText("Flight distance:", hwidth, (textSize * 3), textPaint);
            canvas.drawText((int)m_rocket.getFlightDistance() + " km",
                    hwidth, (textSize * 3) + (textSize/2), textPaint);
            textPaint.setTextSize(m_textSize);
        }
    }

    @Override
    public void present(Canvas canvas, float timeDelta) {
        m_game.getBackground().doDraw(canvas);
        m_system.doDraw(canvas, timeDelta);
        if (m_state != State.CRASH_DELAY && m_state != State.CRASH) {
            m_rocket.doDraw(canvas, timeDelta);
        }
        m_ui.doDraw(canvas);
        m_particles.present(canvas, timeDelta);

        /* Draw text for controls */
        textPaint.setTextSize(m_textSizeHalf);
        if (m_state != State.START) {
            textPaint.setColor(Color.DKGRAY);
        }
        int textX = uicalign + (uicsize/2);
        canvas.drawText("power", textX, m_cy_power + uicsize + m_textSizeHalf, textPaint);
        canvas.drawText("angle", textX, m_cy_angle + uicsize + m_textSizeHalf, textPaint);
        canvas.drawText("launch", textX, m_cy_launch + uicsize + m_textSizeHalf, textPaint);
        textPaint.setTextSize(m_textSize);
        textPaint.setColor(Color.GREEN);

        /* Draw pause */
        if (m_state == State.PAUSE_LAUNCH || m_state == State.PAUSE_START ||
                m_state == State.LOST ||
                m_state == State.CRASH || m_state == State.TARGET ) {
            drawPause(canvas);
            m_pauseui.doDraw(canvas);
        }
    }

    public void clickDown(float x, float y) {
        if (m_pauseui != null) {
            m_pauseui.downHandle(x, y);
        } else {
            m_ui.downHandle(x, y);
        }
    }

    public void clickMove(float x, float y) {
        if (m_pauseui != null) {
            m_pauseui.moveHandle(x, y);
        } else {
            m_ui.moveHandle(x, y);
        }
    }

    public void clickUp(float x, float y) {
        if (m_pauseui != null) {
            m_pauseui.upHandle(x, y);
        } else {
            m_ui.upHandle(x, y);
        }
    }

    @Override
    public void uiAction(int action, int id, int value) {
        if (id == ACTION_LAUNCH) {
            setState(State.LAUNCH);
        }
        if (id == ACTION_CONTINUE) {
            if (m_mission < 5) {
                m_mission = m_mission + 1;
                setState(State.REQUEST_RETRY);
            } else {
                m_game.getAudio().stopAll();
                m_game.setScreen(new SystemCompleteScreen(m_game, m_level));
            }
        }
        if (id == ACTION_PAUSE) {
            setState(State.REQUEST_PAUSE);
        }
        if (id == ACTION_PAUSECONTINUE) {
            setState(State.REQUEST_CONTINUE);
        }
        if (id == ACTION_RETRY) {
            setState(State.REQUEST_RETRY);
        }
        if (id == ACTION_MENU) {
            m_game.getAudio().stopAll();
            m_game.setScreen(new MainMenuScreen(m_game));
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        //Debug.startMethodTracing("orbit");

    }

    @Override
    public void dispose() {
        //Debug.stopMethodTracing();

        // Free up particle effects
        m_system.dispose();
        m_rocket.dispose();
        m_particles.reset();
    }

}
