package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.util.AssetRead;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

public class SpaceBackground {
	private Bitmap m_bg;
	private final GameManager m_game;
	
	public SpaceBackground(GameManager game) {
		m_game = game;
	}

	public void createBackground(int canvasWidth, int canvasHeight) {
		int x = 0;
		int y = 0;
		m_bg = Bitmap.createBitmap(canvasWidth, canvasHeight, Bitmap.Config.RGB_565);
		m_bg.eraseColor(Color.BLACK);
		
		Canvas c = new Canvas(m_bg);
		Bitmap tile = AssetRead.getBitmapFromAsset(m_game.getContext(), "ne1.jpg");

		while (y < canvasHeight) {
			while (x < canvasWidth) {
				c.drawBitmap(tile, x, y, null);
				x = x + tile.getWidth();
			}
			y = y + tile.getHeight();
		}
	}
	
	void doDraw(Canvas canvas) {
		canvas.drawBitmap(m_bg, 0, 0, null);
	}
}
