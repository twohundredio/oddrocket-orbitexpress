package com.oddrocket.orbitexpress;

import com.oddrocket.orbitexpress.gfw.GameManager;
import com.oddrocket.orbitexpress.gfw.Highscores;
import com.oddrocket.orbitexpress.ui.Button;

import android.graphics.Canvas;
import android.graphics.Paint.Align;

public class SystemCompleteScreen extends MenuScreen {
	private static final int ACTION_MENU = 89;
	private final int m_level;
	
	public SystemCompleteScreen(GameManager m_game, int level) {
		super(m_game);
		setTitle("system complete");
		setTitleAlign(Align.CENTER);
		m_level = level;
		
		Button b;
		int bx = (canvasWidth / 2) - (m_buttonWidth / 2);
		int by = canvasHeight - m_buttonHeight - m_buttonHeight;
		b = new Button(bx, by, m_buttonWidth, m_buttonHeight, ACTION_MENU);
		b.setStyle(Button.STYLE_TEXT);
		b.setText("main menu");
		b.setTypeface(m_typeface);
		m_ui.addComponent(b);
	}

	public void present(Canvas canvas, float timeDelta) {
		super.present(canvas, timeDelta);
		
		Highscores hs = m_game.getHighscores();
		String text;
		int theight = m_textSize / 2;
		int ty;
		m_headerPaint.setTextSize(theight);
		m_headerPaint.setTextAlign(Align.CENTER);
		int x = canvasWidth / 2;
		
		ty = (canvasHeight / 2) - ((theight*6)/2);
		
		text = "Shortest total distance:";
		canvas.drawText(text, x, ty, m_headerPaint);
		ty = ty + theight;
		text = hs.getLevelScoreMin1(m_level) + " km";
		canvas.drawText(text, x, ty, m_headerPaint);
		
		/*
		ty = ty + theight + hheight;
		text = "Shortest time:";
		canvas.drawText(text, x, ty, m_headerPaint);
		ty = ty + theight;
		text = hs.getLevelScoreMin2(m_level) + " days";
		canvas.drawText(text, x, ty, m_headerPaint);
		*/
		
		ty = ty + theight + theight;
		text = "Longest total distance:";
		canvas.drawText(text, x, ty, m_headerPaint);
		ty = ty + theight;
		text = hs.getLevelScoreMax1(m_level) + " km";
		canvas.drawText(text, x, ty, m_headerPaint);
		
		/*
		ty = ty + theight + hheight;
		text = "Longest time:";
		canvas.drawText(text, x, ty, m_headerPaint);
		ty = ty + theight;
		text = hs.getLevelScoreMax2(m_level) + " days";
		canvas.drawText(text, x, ty, m_headerPaint);
		*/
		
		m_headerPaint.setTextSize(m_textSize);
		m_headerPaint.setTextAlign(Align.LEFT);
	}
	
	public void uiAction(int action, int id, int value) {
		if (id == ACTION_MENU) {
			m_game.setScreen(new MainMenuScreen(m_game));
		}
	}
}
