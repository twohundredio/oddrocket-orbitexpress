package com.oddrocket.orbitexpress.gfw;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import java.util.EnumMap;
import java.util.Map;

public class Audio {
    GameManager gameManager;
    SoundPool  soundPool;

    public enum Sounds { LEVEL_COMPLETE, EXPLOSION, LAUNCH };

    private class SoundEffect {
        int playId = -1;
        int streamId = -1;
    }

    private Map<Sounds, SoundEffect> soundEffects= new EnumMap<Sounds, SoundEffect>(Sounds.class);

    public Audio(GameManager gameManager) {
        this.gameManager = gameManager;

        gameManager.getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNewSoundPool();
        } else {
            createOldSoundPool();
        }

        for (Sounds s : Sounds.values()) {
            soundEffects.put(s, new SoundEffect());
        }

        loadSounds();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void createNewSoundPool(){
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .build();
    }

    @SuppressWarnings("deprecation")
    protected void createOldSoundPool(){
        soundPool = new SoundPool(5,AudioManager.STREAM_MUSIC,0);
    }

    void loadSounds() {
        try {
            AssetManager assetManager = gameManager.getActivity().getAssets();
            SoundEffect effect;

            effect = soundEffects.get(Sounds.LEVEL_COMPLETE);
            AssetFileDescriptor d = assetManager.openFd("complete.ogg");
            effect.playId = soundPool.load(d, 0);

            effect = soundEffects.get(Sounds.EXPLOSION);
            d = assetManager.openFd("explosion.ogg");
            effect.playId = soundPool.load(d, 0);

            effect = soundEffects.get(Sounds.LAUNCH);
            d = assetManager.openFd("launch.ogg");
            effect.playId = soundPool.load(d, 0);
        } catch(Exception e) {

        }
    }

    public void playSound(Sounds sound) {
        stopAll();

        if (gameManager.getSettings().soundEnabled()) {
            SoundEffect effect = soundEffects.get(sound);
            if (effect.playId != -1) {
                effect.streamId = soundPool.play(effect.playId, 1.0f, 1.0f, 0, 0, 1);
            }
        }
    }

    public void stopAll() {
        for (Sounds s : Sounds.values()) {
            stop(s);
        }
    }

    public void stop(Sounds s) {
        SoundEffect effect = soundEffects.get(s);
        if (effect.streamId != -1) {
            soundPool.stop(effect.streamId);
        }
    }

    public void pause() {
        soundPool.autoPause();
    }

    public void resume() {
        soundPool.autoResume();
    }
}
