package com.oddrocket.orbitexpress.gfw;

import com.oddrocket.orbitexpress.MainMenuScreen;

import android.graphics.Canvas;

public class BlankScreen implements Screen {
	private final GameManager m_game;
	
	public BlankScreen(GameManager game) {
		m_game = game;
	}

	@Override
	public void update(float timeDelta) {
		if (m_game.getRenderer().screenSizeKnown()) {
			m_game.generateBackground();
			m_game.setScreen(new MainMenuScreen(m_game));
		}
	}

	@Override
	public void present(Canvas canvas, float timeDelta) {
		
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

	@Override
	public void clickDown(float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickMove(float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickUp(float x, float y) {
		// TODO Auto-generated method stub
		
	}

}
