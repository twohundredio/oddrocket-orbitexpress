package com.oddrocket.orbitexpress.gfw;

public class Collision {

	public static boolean pointCollision(float x1, float y1, 
			float x2, float y2, float width2, float height2) {
		return (x1 >= x2) && (x1 <= (x2 + width2)) &&
				(y1 >= y2) && (y1 <= (y2 + height2));
	}
	
	public static boolean boxCollision(float x1, float y1, float width1, float height1,
			float x2, float y2, float width2, float height2) {
		return x1 < x2 + width2 && x1 + width1 > x2 &&
				y1 < y2 + height2 && height1 + y1 > y2;
	}
	
	public static boolean circleCollision(float x1, float y1, float r1,
			float x2, float y2, float r2) {
		float dx = x1 - x2;
		float dy = y1 - y2;
		double distance = Math.sqrt(dx * dx + dy * dy);

		return distance < r1 + r2;

	}
}
