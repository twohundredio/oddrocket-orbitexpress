package com.oddrocket.orbitexpress.gfw;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.oddrocket.orbitexpress.SpaceBackground;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.widget.RelativeLayout;

public class GameManager {
    private Screen m_screen;
    private GameRenderView m_renderer;
    private GameTouchInput m_touchInput;
    private final Highscores m_highscores;
    private final GameSettings m_settings;
    private final Activity m_act;
    private final Context m_ctx;
    private Typeface m_typeface = null;
    private final SpaceBackground m_spacebg;
    private SharedPreferences m_preferences;
    private final Textures m_textures;
    private final ParticlePool m_particlePool = new ParticlePool();
    private AdView adView;
    private boolean adEnabled = false;
    private boolean adShouldDisplay = false;
    private Audio m_audio;

    public GameManager(Activity act) {
        m_act = act;
        m_ctx = act.getApplicationContext();
        m_highscores = new Highscores(4, 5);
        m_settings = new GameSettings();
        m_spacebg = new SpaceBackground(this);
        m_textures = new Textures(this);
    }

    public GameSettings getSettings() {
        return m_settings;
    }

    public Highscores getHighscores() {
        return m_highscores;
    }

    public Context getContext() {
        return m_ctx;
    }

    public Activity getActivity() {
        return m_act;
    }

    public synchronized Typeface getTypeface() {
        if (m_typeface == null) {
            AssetManager assetManager = getContext().getAssets();
            m_typeface = Typeface.createFromAsset(assetManager, "neuropolx.ttf");
        }
        return m_typeface;
    }

    public void generateBackground() {
        int canvasWidth = getRenderer().getScreenWidth();
        int canvasHeight = getRenderer().getScreenHeight();

        m_spacebg.createBackground(canvasWidth, canvasHeight);
        m_textures.loadTextures();
    }

    public SpaceBackground getBackground() {
        return m_spacebg;
    }

    public synchronized void setScreen(Screen screen) {
        if (m_screen != null) {
            m_screen.pause();
            m_screen.dispose();
        }
        m_screen = screen;
        m_screen.resume();
        m_screen.update(0);
    }

    public synchronized Screen getCurrentScreen() {
        return m_screen;
    }

    public void setRenderer(GameRenderView renderer) {
        m_renderer = renderer;
    }

    public GameRenderView getRenderer() {
        return m_renderer;
    }

    public void setTouchInput(GameTouchInput touchInput) {
        m_touchInput = touchInput;
    }

    public void onPause() {
        m_renderer.pause();
    }

    public void onResume() {
        m_renderer.resume();
    }

    public void setPreferences(SharedPreferences p) {
        m_preferences = p;
    }

    public void saveSettings() {
        boolean soundEnabled;
        String highscore;
        Editor e = m_preferences.edit();

        soundEnabled = m_settings.soundEnabled();
        e.putBoolean("sound", soundEnabled);

        highscore = m_highscores.save();
        e.putString("hs", highscore);

        e.apply();
    }

    public void loadSettings() {
        boolean soundEnabled;
        String highscore;

        soundEnabled = m_preferences.getBoolean("sound", true);
        m_settings.setSoundEnabled(soundEnabled);

        highscore = m_preferences.getString("hs", null);
        if (highscore != null) {
            m_highscores.load(highscore);
        }
    }

    public Textures getTextures() {
        return m_textures;
    }

    public ParticlePool getParticlePool() {
        return m_particlePool;
    }

    public boolean isAdEnabled() {
        return adEnabled;
    }

    public void configureAds() {
        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(m_act) !=
                ConnectionResult.SUCCESS) {
            adEnabled = false;
        }
        if (adEnabled) {
            adView = new AdView(m_act);
            adView.setAdUnitId("");
            adView.setAdSize(AdSize.SMART_BANNER);

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

            adView.setLayoutParams(lp);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    loadedAd();
                }
            });

            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice("")
                    .build();
            adView.loadAd(adRequest);
        }
    }

    public synchronized AdView getAdView() {
        return adView;
    }

    public synchronized void loadedAd() {
        if (adShouldDisplay) {
            showAds();
        }
    }

    public synchronized void showAds() {
        if (adView != null) {
            adShouldDisplay = true;
            m_act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adView.setVisibility(View.GONE);
                        adView.setVisibility(View.VISIBLE);
                    } catch(Exception e) {

                    }
                }
            });
        }
    }

    public synchronized void hideAds() {
        if (adView != null) {
            adShouldDisplay = false;
            m_act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        adView.setVisibility(View.GONE);
                    } catch(Exception e) {

                    }
                }
            });
        }
    }

    public synchronized void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
    }

    public Audio getAudio() {
        return m_audio;
    }

    public void setAudio(Audio audio) {
        m_audio = audio;
    }
}