package com.oddrocket.orbitexpress.gfw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameRenderView extends SurfaceView implements Runnable {
	private final GameManager m_game;
	private final SurfaceHolder m_holder;
	private volatile boolean m_running = false;
	private Thread m_renderThread = null;
	private boolean m_knownSize;
	private int m_width;
	private int m_height;
	private int m_realWidth;
	private int m_realHeight;
	private Bitmap m_bitmap;
	private Canvas m_canvas;
	private float fps = 0;
	private final boolean displayFPS = false;
	private int frames = 0;
	private float time = 0;
	private Paint m_textPaint;
	
	public GameRenderView(Context context, GameManager game) {
		super(context);
		
		m_holder = getHolder();
		m_game = game;
		
		if (displayFPS) {
			m_textPaint = new Paint();
			m_textPaint.setColor(Color.WHITE);
			m_textPaint.setTextSize(20); 
		}
	}
	
	public boolean screenSizeKnown() {
		return m_knownSize;
	}
	
	public int getScreenWidth()
	{
		return m_width;
	}
	
	public int getScreenHeight()
	{
		return m_height;
	}

	@Override
	public void run() {
		long startTime = System.nanoTime();
		while (m_running) {
			if (!m_holder.getSurface().isValid())
				continue;
			
			float deltaTime = (System.nanoTime() - startTime) / 1000000000.0f;
			startTime = System.nanoTime();
			
			Canvas canvas = m_holder.lockCanvas();

			if (!m_knownSize) {
				m_realWidth = getWidth();
				m_realHeight = getHeight();
				if (m_realWidth > 0 && m_realHeight > 0) {
					m_knownSize = true;
					setScreenSize();
					createBitmap();
				}
			}
			
			canvas.drawRGB(0, 0, 0);
			m_game.getCurrentScreen().update(deltaTime);
			
			if (m_knownSize) {
				m_game.getCurrentScreen().present(m_canvas, deltaTime);
				canvas.drawBitmap(m_bitmap, getXOffset(), getYOffset(), null);
				
				if (displayFPS) {
					canvas.drawText("FPS: " + fps, 0, 20, m_textPaint);
					
					frames++;
					time += deltaTime;
					if(time > 1) {
						fps = (frames/time);
						frames = 0;
						time = 0;
					}
				}
			}
			
			m_holder.unlockCanvasAndPost(canvas);
		}
	}
	
	public int getYOffset() {
		if (m_height != m_realHeight) {
			return (m_realHeight / 2) - (m_height / 2);
		} else {
			return 0;
		}
	}
	
	public int getXOffset() {
		if (m_width != m_realWidth) {
			return (m_realWidth / 2) - (m_width / 2);
		} else {
			return 0;
		}
	}
	
	private void setScreenSize() {
		m_width = m_realWidth;
		m_height = m_realHeight;
		double ratio = m_width/m_height;
		if (ratio < 1.6) {
			m_height = (int) (m_width / 1.6);
		} else if (ratio >= 1.7) {
			m_width = (int) (m_height * 1.6);
		}
	}

	private void createBitmap() {
		m_bitmap = Bitmap.createBitmap(getScreenWidth(), getScreenHeight(), Bitmap.Config.RGB_565);
		m_canvas = new Canvas(m_bitmap);
	}

	public void resume() {
		m_running = true;
		m_renderThread = new Thread(this);
		m_renderThread.start();
	}
	
	public void pause() {
		m_running = false;
		if (m_renderThread != null) {
			while (true) {
				try {
					m_renderThread.join();
					return;
				} catch (InterruptedException e) {
					// Retry
				}
			}
		}
	}
}
