package com.oddrocket.orbitexpress.gfw;

public class GameSettings {
	private boolean m_sound = true;
	
	public void setSoundEnabled(boolean enabled) {
		m_sound = enabled;
	}
	
	public boolean soundEnabled() {
		return m_sound;
	}

}
