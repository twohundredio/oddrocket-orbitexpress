package com.oddrocket.orbitexpress.gfw;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class GameTouchInput implements OnTouchListener {
	private final GameManager m_game;
	
	public GameTouchInput(GameManager game) 
	{
		m_game = game;
		game.getRenderer().setOnTouchListener(this);
	}
	
	private boolean onTouchEvent(MotionEvent e) {
		float touchX = e.getX() - m_game.getRenderer().getXOffset();
		float touchY = e.getY() - m_game.getRenderer().getYOffset();
		switch (e.getAction()) {
		case MotionEvent.ACTION_DOWN:
			m_game.getCurrentScreen().clickDown(touchX, touchY);
			break;
		case MotionEvent.ACTION_MOVE:
			m_game.getCurrentScreen().clickMove(touchX, touchY);
			break;
		case MotionEvent.ACTION_UP:
			m_game.getCurrentScreen().clickUp(touchX, touchY);
			break;
		}
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return onTouchEvent(event);
	}
}
