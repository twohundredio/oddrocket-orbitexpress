package com.oddrocket.orbitexpress.gfw;

import java.util.Vector;

public class Highscores {
	class Score {
		boolean set = false;
		int min1 = 0;
		int max1 = 0;
		int min2 = 0;
		int max2 = 0;
	}
	
	private final Vector<Vector<Score>> m_levels;
	
	public Highscores(int levels, int stages) {
		m_levels = new Vector<>();
		for (int l = 0; l < levels; l++) {
			m_levels.add(new Vector<Score>());
			for (int s = 0; s < stages; s++) {
				m_levels.get(l).add(new Score());
			}
		}
	}
	
	public String save() {
		String str = "";
	
		for (Vector<Score> l : m_levels) {
			for (Score s : l) {
				str = str.concat(Boolean.toString(s.set));
				str = str.concat(",");
				str = str.concat(Integer.toString(s.min1));
				str = str.concat(",");
				str = str.concat(Integer.toString(s.max1));
				str = str.concat(",");
				str = str.concat(Integer.toString(s.min2));
				str = str.concat(",");
				str = str.concat(Integer.toString(s.max2));
				str = str.concat(",");
			}
		}
		
		return str;
	}
	
	public void load(String hs) {
		String[] scores = hs.split(",");
		int idx = 0;
		
		for (Vector<Score> l : m_levels) {
			for (Score s : l) {
				s.set = Boolean.parseBoolean(scores[idx]);
				idx++;
				s.min1 = Integer.parseInt(scores[idx]);
				idx++;
				s.max1 = Integer.parseInt(scores[idx]);
				idx++;
				s.min2 = Integer.parseInt(scores[idx]);
				idx++;
				s.max2 = Integer.parseInt(scores[idx]);
				idx++;
			}
		}
	}
	
	public boolean isLevelComplete(int level) {
		boolean complete = true;
		for (Score s : m_levels.get(level - 1)) {
			if (!s.set) {
				complete = false;
			}
		}
		return complete;
	}
	
	public int getStageComplete(int level) {
		int i = 0;
		for (Score s : m_levels.get(level - 1)) {
			if (!s.set) {
				return i;
			}
			i++;
		}
		return i;
	}
	
	public void setScore(int level, int stage, int score1, int score2) {
		Score s = m_levels.get(level - 1).get(stage - 1);
		if (!s.set) {
			s.set = true;
			s.max1 = s.min1 = score1;
			s.max2 = s.min2 = score2;
		} else {
			if (score1 > s.max1) {
				s.max1 = score1;
			} else if (score1 < s.min1) {
				s.min1 = score1;
			}
			
			if (score2 > s.max2) {
				s.max2 = score2;
			} else if (score2 < s.min2) {
				s.min2 = score2;
			}
		}
	}
	
	public int getLevelScoreMin1(int level) {
		int score = 0;
		for (Score s : m_levels.get(level - 1)) {
			score = score + s.min1;
		}
		return score;
	}
	
	public int getLevelScoreMin2(int level) {
		int score = 0;
		for (Score s : m_levels.get(level - 1)) {
			score = score + s.min2;
		}
		return score;
	}
	
	public int getLevelScoreMax1(int level) {
		int score = 0;
		for (Score s : m_levels.get(level - 1)) {
			score = score + s.max1;
		}
		return score;
	}
	
	public int getLevelScoreMax2(int level) {
		int score = 0;
		for (Score s : m_levels.get(level - 1)) {
			score = score + s.max2;
		}
		return score;
	}
}
