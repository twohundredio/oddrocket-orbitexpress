package com.oddrocket.orbitexpress.gfw;

import android.graphics.Color;

public class Particle {
	private float x;
	private float y;
	private float life;
	private float olife;
	private float angle;
	private int alpha;
	private float xs;
	private float ys;
	private float size;
	private float osize;
	private float red;
	private float green;
	private float blue;
	private float cred;
	private float cgreen;
	private float cblue;
	private int ered;
	private int egreen;
	private int eblue;
	
	public Particle() {
		
	}

	public void reset(float x, float y, float life, float angle, float size, float speed) {
		this.x = x;
		this.y = y;
		olife = this.life = life;
		this.angle = angle;
		this.osize = this.size = size;
		
		xs = (float) (speed * Math.cos(Math.toRadians(angle)));
		ys = (float) (speed * Math.sin(Math.toRadians(angle)));
		setColor(Color.GRAY);
	}
	
	private void setColor(int colour) {
		red = Color.red(colour);
		green = Color.green(colour);
		blue = Color.blue(colour);
		
		cred = ered = 0;
		cgreen = egreen = 0;
		cblue = eblue = 0;
	}
	
	public void setColorChange(int startColour, int endColour) {
		setColor(startColour);
		
		ered = Color.red(endColour);
		egreen = Color.green(endColour);
		eblue = Color.blue(endColour);
		
		cred = (Color.red(endColour) - red) / olife * 2;
		cblue = (Color.blue(endColour) - blue) / olife * 2;
		cgreen = (Color.green(endColour) - green) / olife * 2;
	}
	
	public int getColor() {
		return Color.argb(alpha, Math.max((int)red, ered), Math.max((int)green, egreen), Math.max((int)blue,eblue));
	}
	
	public float getSize() { 
		return size;
	}
	
	public float getOSize() { 
		return osize;
	}
	
	public float getAngle() { 
		return angle;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public void update(float dt) {
		this.life -= dt;
	    
    	if(this.life > 0) {
    		float ageRatio = this.life / this.olife;
    		this.size = this.osize * ageRatio;
    		alpha = (int) (ageRatio * 255);
    		this.x += this.xs * dt;
    		this.y += this.ys * dt;
    		red += cred * dt;
    		green += cgreen * dt;
    		blue += cblue * dt;
    	}
	}
	
	public boolean isAlive() {
		return life > 0;
	}
}
