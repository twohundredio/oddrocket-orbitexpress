package com.oddrocket.orbitexpress.gfw;

import java.util.ArrayList;
import java.util.Random;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class ParticleEffect {
    private final ArrayList<Particle> particles = new ArrayList<>();
    private final Paint paint = new Paint();
    private final Random r = new Random();
    private final ParticlePool pool;
    private boolean reuse = false;
    private float x;
    private float y;
    private float radius;

    public ParticleEffect(GameManager game) {
        pool = game.getParticlePool();
    }

    public synchronized void addTrail(float cx, float cy, float sx, float sy, float ex, float ey, float life, float radius, float width) {
        double sa = Math.toDegrees(Math.atan2(sx - cx, cy - sy));
        double ea = Math.toDegrees(Math.atan2(ex - cx, cy - ey));
        /* Fixup for continuous tail */
        if (sa < 0) {
            sa = 360 + sa;
        }
        if (sa > 360) {
            sa = sa - 360;
        }
        if (ea < 0) {
            ea = 360 + ea;
        }
        if (ea > 360) {
            ea = ea - 360;
        }
        if (ea < sa) {
            ea += 360;
        }
        double c = 2 * Math.PI * radius;
        int points = (int) (c * (ea-sa)/360);
        double inc = (ea - sa)/points;
        Particle p;
        width = (float) (width * 0.4);
        /* Adjust for drawing */
        sa = sa - 90;
        int pointsAbs = Math.abs(points);
        /* Add particles */
        for (double i = 0; i < pointsAbs; i++) {
            float x = (float) (cx + (Math.cos(Math.toRadians(sa)) * radius));
            float y = (float) (cy + (Math.sin(Math.toRadians(sa)) * radius));
            p = pool.getParticle();
            p.reset(x, y, life, 0, width, 0);
            particles.add(p);

            sa = sa + inc;
        }
    }

    public synchronized void addRocket(float sx, float sy, float ex, float ey, float size, float life) {
        int points = (int) (Math.sqrt((Math.pow(sx - ex, 2) + Math.pow(sy - ey, 2))));
        Particle p;
        points = Math.max(points, 1);
        for (double i = 0; i < points; i++) {
            float x = (float) (sx + (sx - ex)/points*i);
            float y = (float) (sy + (sy - ey)/points*i);
            p = pool.getParticle();
            p.reset(x, y, life, 0, size, 0);
            p.setColorChange(Color.rgb(255, 255, 255), Color.rgb(255, 128, 0));
            particles.add(p);
        }
    }

    public synchronized void addExplosion(float x, float y, float radius, int time) {
        Particle p;
        for (int i = 0; i < 80; i++) {
            p = pool.getParticle();
            float life = 1 + r.nextInt(time);
            float angle = r.nextInt(360);
            float speed = 1 + r.nextInt((int)(radius*2));
            p.reset(x, y, life, angle, radius, speed);
            p.setColorChange(Color.rgb(255, 255, 255), Color.rgb(255, 128, 0));
            particles.add(p);
        }
    }

    public synchronized void addSun(float x, float y, float radius) {
        final int STEPS = 180;
        reuse = true;
        this.x = x;
        this.y = y;
        this.radius = radius;
        double c = 2 * Math.PI * radius;
        float size = (float) (c / STEPS);
        Particle p;
        for (int i = 0; i < STEPS; i++) {
            p = pool.getParticle();
            double angle = 360/STEPS * i;
            float px = (float) (x + (Math.cos(Math.toRadians(angle)) * radius));
            float py = (float) (y + (Math.sin(Math.toRadians(angle)) * radius));
            float life = 6 * r.nextFloat();
            float speed = (float) ((radius/3.5/6) * r.nextFloat());
            p.reset(px, py, life, (float) angle, size, speed);
            p.setColorChange(Color.rgb(255,215,0), Color.rgb(255, 128, 0));
            particles.add(p);
        }

        update(0.5f);
    }

    private void resetSunParticle(Particle p) {
        float angle = p.getAngle();
        float px = (float) (x + (Math.cos(Math.toRadians(angle)) * radius));
        float py = (float) (y + (Math.sin(Math.toRadians(angle)) * radius));
        float life = 6 * r.nextFloat();
        float speed = (float) ((radius/3.5/6) * r.nextFloat());
        float size = p.getOSize();
        p.reset(px, py, life, angle, size, speed);
        p.setColorChange(Color.rgb(255, 255, 255), Color.rgb(255, 128, 0));
    }

    public synchronized void update(float dt) {
        int size = particles.size();
        for (int i = 0; i < size; i++, size = particles.size()) {
            Particle p = particles.get(i);
            if (p.isAlive()) {
                p.update(dt);
            } else {
                if (reuse) {
                    resetSunParticle(p);
                } else {
                    if (i < size - 1) {
                        particles.set(i, particles.remove(size - 1));
                    } else {
                        particles.remove(i);
                    }
                    pool.returnParticle(p);
                    i--;
                }
            }
        }
    }

    public synchronized void reset() {
        while (!particles.isEmpty()) {
            pool.returnParticle(particles.remove(particles.size() - 1));
        }
    }

    public synchronized void present(Canvas canvas, float timeDelta) {
        for (Particle p : particles) {
            if (p.isAlive()) {
                paint.setColor(p.getColor());
                canvas.drawCircle(p.getX(), p.getY(), p.getSize(), paint);
            }
        }
    }
}
