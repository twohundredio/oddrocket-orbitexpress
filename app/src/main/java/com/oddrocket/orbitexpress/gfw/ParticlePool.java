package com.oddrocket.orbitexpress.gfw;

import java.util.ArrayList;

public class ParticlePool {
	private final ArrayList<Particle> particles = new ArrayList<>();
	
	public synchronized Particle getParticle() {
		if (particles.isEmpty()) {
			return new Particle();
		} 
		
		return particles.remove(particles.size() - 1);
	}
	
	public synchronized void returnParticle(Particle p) {
		particles.add(p);
	}
}
