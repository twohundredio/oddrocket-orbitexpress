package com.oddrocket.orbitexpress.gfw;

public class Physics {
	class Vec {
		public double x;
		public double y;
	}
	
	public Vec getGForce(double x1, double y1, double m1, double x2, double y2, double m2) {
		Vec result = new Vec();
		
		double xd = x2 - x1;
		double yd = y2 - y1;
		double distance = Math.sqrt(Math.pow(Math.abs(xd), 2) + Math.pow(Math.abs(yd), 2));
		
		double radius = distance / 2;
		double force = ((m1 * m2) / Math.pow(radius, 2));
		
		double t = Math.atan2(yd, xd);
		
		result.y = Math.sin(t) * force;
		result.x = Math.cos(t) * force;
		
		return result;
	}
}
