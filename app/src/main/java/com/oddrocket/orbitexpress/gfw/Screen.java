package com.oddrocket.orbitexpress.gfw;

import android.graphics.Canvas;

public interface Screen {
	void update(float timeDelta);
	void present(Canvas canvas, float timeDelta);
	void pause();
	void resume();
	void dispose();
	
	void clickDown(float x, float y);
	void clickMove(float x, float y);
	void clickUp(float x, float y);
}
