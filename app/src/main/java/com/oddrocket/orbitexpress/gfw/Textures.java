package com.oddrocket.orbitexpress.gfw;

import com.oddrocket.orbitexpress.util.AssetRead;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;

public class Textures {
	private final GameManager m_game;
	private final Bitmap[] planetBitmaps = new Bitmap[4];
	private final BitmapShader[] planetBitmapShaders = new BitmapShader[4];
	
	Textures(GameManager game) {
		m_game = game;
	}
	
	public void loadTextures() {
		planetBitmaps[0] = AssetRead.getBitmapFromAsset(m_game.getContext(), "p1.jpg");
		planetBitmapShaders[0] = new BitmapShader(planetBitmaps[0], BitmapShader.TileMode.REPEAT, BitmapShader.TileMode.REPEAT);
		
		planetBitmaps[1] = AssetRead.getBitmapFromAsset(m_game.getContext(), "p2.jpg");
		planetBitmapShaders[1] = new BitmapShader(planetBitmaps[1], BitmapShader.TileMode.REPEAT, BitmapShader.TileMode.REPEAT);

		planetBitmaps[2] = AssetRead.getBitmapFromAsset(m_game.getContext(), "p3.jpg");
		planetBitmapShaders[2] = new BitmapShader(planetBitmaps[2], BitmapShader.TileMode.REPEAT, BitmapShader.TileMode.REPEAT);

		planetBitmaps[3] = AssetRead.getBitmapFromAsset(m_game.getContext(), "p4.jpg");
		planetBitmapShaders[3] = new BitmapShader(planetBitmaps[3], BitmapShader.TileMode.REPEAT, BitmapShader.TileMode.REPEAT);
	}
	
	public BitmapShader getBitmapShader(int index) {
		return planetBitmapShaders[index];
	}
}
