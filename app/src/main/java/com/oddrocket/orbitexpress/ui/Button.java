package com.oddrocket.orbitexpress.ui;

import com.oddrocket.orbitexpress.gfw.Collision;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Typeface;

public class Button 
	extends UiComponent {
	private final int m_x;
	private final int m_y;
	private final int m_width;
	private final int m_height;
	private boolean m_selected = false;
	private boolean m_active = true;
	private final Paint paint = new Paint();
	
	private String m_text;
	private Typeface m_typeface;
	
	private IButtonRender m_render = null;
	
	private static final int STYLE_ROUND = 1;
	public static final int STYLE_TEXT = 2;
	
	private int m_style = STYLE_ROUND;
	
	public Button(int x, int y, int width, int height, int actionId) {
		super(x, y, width, height, actionId);
		m_x = x;
		m_y = y;
		m_width = width;
		m_height = height;
		m_actionId = actionId;
	}
	
	public void setRender(IButtonRender render) {
		render.setLocation(m_x, m_y);
		render.setSize(m_width, m_height);
		m_render = render;
	}
	
	public void setSelected(boolean selected) {
		m_selected = selected;
	}
	
	public void setInactive() {
		m_active = false;
		if (m_render != null) {
			m_render.setInactive();
		}
	}
	
	public void setActive() {
		m_active = true;
		if (m_render != null) {
			m_render.setActive();
		}
	}
	
	public void setStyle(int style) {
		m_style = style;
	}
	
	public void setText(String text) {
		m_text = text;
	}
	
	public void setTypeface(Typeface typeface) {
		m_typeface = typeface;
	}
	
	public boolean clickHandle(float x, float y) {
		return m_active && Collision.pointCollision(x, y, m_x, m_y, m_width, m_height);
	}
	
	public void doDraw(Canvas canvas) {
		if (m_render != null) {
			m_render.doDraw(canvas);
		} else if (m_style == STYLE_ROUND) {
			doDrawRound(canvas);
		} else if (m_style == STYLE_TEXT) {
			doDrawText(canvas);
		}
	}
	
	private void doDrawText(Canvas canvas) {	
		int textHeight = (int) (m_height * 0.8);
		
		Paint paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setStyle(Style.STROKE);
		
		Paint textPaint;
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setTypeface(m_typeface);
		textPaint.setColor(Color.GREEN);
		textPaint.setTextSize(textHeight); 
		textPaint.setTextAlign(Align.CENTER);
		if (m_selected) {
			paint.setStyle(Style.FILL_AND_STROKE);
			textPaint.setColor(Color.BLACK);
		}
		if (!m_active) {
			paint.setColor(Color.DKGRAY);
			textPaint.setColor(Color.DKGRAY);
		}
		
		canvas.drawRect(m_x, m_y, m_x + m_width, m_y + m_height, paint);
		canvas.drawText(m_text, m_x + (m_width/2), m_y + textHeight, textPaint);
	}
	
	private void doDrawRound(Canvas canvas) {
		int x_offset = m_width / 2;
		int y_offset = m_height / 2;
		
		if (m_active) {
			paint.setColor(Color.LTGRAY);
		} else {
			paint.setColor(Color.DKGRAY);
		}
		paint.setStyle(Style.FILL);
		
		canvas.drawCircle(m_x + x_offset, m_y + y_offset, x_offset, paint);
	}
}
