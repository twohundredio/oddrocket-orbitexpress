package com.oddrocket.orbitexpress.ui;

import android.graphics.Canvas;

public class ButtonRender implements IButtonRender {
	int m_x;
	int m_y;
	int m_width;
	int m_height;
	boolean m_active = true;

	@Override
	public void setLocation(int x, int y) {
		m_x = x;
		m_y = y;
	}

	@Override
	public void setSize(int width, int height) {
		m_width = width;
		m_height = height;
	}

	@Override
	public void doDraw(Canvas canvas) {
	}

	@Override
	public void setInactive() {
		m_active = false;
	}

	@Override
	public void setActive() {
		m_active = true;
	}
}
