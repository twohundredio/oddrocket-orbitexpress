package com.oddrocket.orbitexpress.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;

public class ButtonRenderNext extends ButtonRender {
	private final Paint paint = new Paint();
	
	@Override
	public void doDraw(Canvas canvas) {
		int x_offset = m_width / 2;
		int y_offset = m_height / 2;
		float iconsize = (int) (m_width * 0.5);
		float iconhalf = iconsize / 2;
		int dx = m_width/3;
		
		if (m_active) {
			paint.setColor(Color.GREEN);
		} else {
			paint.setColor(Color.DKGRAY);
		}
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(2);
		
		canvas.drawCircle(m_x + x_offset, m_y + y_offset, x_offset, paint);
		
		paint.setStyle(Style.FILL);
		Path p = new Path();
		p.moveTo(0, 0);
		p.rLineTo(iconsize, iconhalf);
		p.rLineTo(-iconsize, iconhalf);
		p.rLineTo(0, -iconsize);
		p.close();
		p.offset(m_x + dx, m_y + ((m_height-iconsize)/2));
		
		canvas.drawPath(p, paint);
	}
	
}
