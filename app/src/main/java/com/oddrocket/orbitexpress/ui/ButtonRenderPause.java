package com.oddrocket.orbitexpress.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class ButtonRenderPause extends ButtonRender {
	private final Paint paint = new Paint();
	
	public ButtonRenderPause() {
		paint.setColor(Color.GREEN);
	}
	
	@Override
	public void doDraw(Canvas canvas) {
		int x_offset = m_width / 2;
		int y_offset = m_height / 2;
		
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(2);
		
		canvas.drawCircle(m_x + x_offset, m_y + y_offset, x_offset, paint);
		
		paint.setStyle(Style.FILL);
		
		int lwidth = (int) (m_width * 0.15);
		int lheight = (int) (m_height * 0.5);
		int ly = m_y + (m_height/2) - (lheight/2);
		int lx1 = m_x + (m_width/2) - lwidth - (lwidth/2);
		int lx2 = lx1 + (lwidth * 2);
		
		if (!m_active) {
			paint.setColor(Color.DKGRAY);
		} else {
			paint.setColor(Color.GREEN);
		}
		
		canvas.drawRect(lx1, ly, lx1 + lwidth, ly + lheight, paint);
		canvas.drawRect(lx2, ly, lx2 + lwidth, ly + lheight, paint);
	}	
}
