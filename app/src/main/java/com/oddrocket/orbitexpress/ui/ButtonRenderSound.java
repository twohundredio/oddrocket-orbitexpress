package com.oddrocket.orbitexpress.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;

public class ButtonRenderSound extends ButtonRender {
	private boolean m_soundEnabled = true;
	private final Paint paint = new Paint();
	private int m_radius;
	private int cx;
	private int cy;
	private int sx1;
	private int sy1;
	private int sx2;
	private int sy2;
	private Path m_speaker;
	private RectF m_rect;
	
	public ButtonRenderSound() {
		paint.setColor(Color.GREEN);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(2);
	}
	
	public void setSize(int width, int height) {
		super.setSize(width, height);
		
		m_radius = m_width / 2;
		cx = m_x + m_radius;
		cy = m_y + m_radius;
		
		sx1 = (int) (m_radius * Math.cos(Math.toRadians(45)) + cx);
		sy1 = (int) (m_radius * Math.sin(Math.toRadians(45)) + cy);
		sx2 = (int) (m_radius * Math.cos(Math.toRadians(180 + 45)) + cx);
		sy2 = (int) (m_radius * Math.sin(Math.toRadians(180 + 45)) + cy);
		
		float iconsize = (int) (m_width * 0.5);
		float iconthird = iconsize / 3;
		float icon4 = iconsize / 4;
		
		m_speaker = new Path();
		m_speaker.moveTo(0, iconthird);
		m_speaker.rLineTo(iconthird, 0);
		m_speaker.rLineTo(iconthird, -icon4);
		m_speaker.rLineTo(0, iconsize);
		m_speaker.rLineTo(-iconthird, -icon4);
		m_speaker.rLineTo(-iconthird, 0);
		
		m_speaker.close();
		m_rect = new RectF();
		m_speaker.computeBounds(m_rect, false);
		m_speaker.offset(cx - (m_rect.right/2), cy - (m_rect.bottom/2));
	}
	
	@Override
	public void doDraw(Canvas canvas) {
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(2);
		canvas.drawCircle(cx, cy, m_radius, paint);
		
		if (!m_soundEnabled) {
			paint.setStrokeWidth(3);
			canvas.drawLine(sx1, sy1, sx2, sy2, paint);
		}
		
		paint.setStyle(Style.FILL);
		canvas.drawPath(m_speaker, paint);
	}	
	
	public void setSoundEnabled(boolean enabled) {
		m_soundEnabled = enabled;
	}
}
