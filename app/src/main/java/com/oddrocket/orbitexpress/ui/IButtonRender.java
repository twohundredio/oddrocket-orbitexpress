package com.oddrocket.orbitexpress.ui;

import android.graphics.Canvas;

interface IButtonRender {
	void setLocation(int x, int y);
	void setSize(int width, int height);
	void doDraw(Canvas canvas);
	void setInactive();
	void setActive();
}
