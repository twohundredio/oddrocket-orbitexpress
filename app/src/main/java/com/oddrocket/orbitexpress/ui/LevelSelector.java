package com.oddrocket.orbitexpress.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class LevelSelector extends UiComponent {
	private static final int STARTVALUE = 75;
	private int m_position = STARTVALUE;
	private final int m_x_offset;
	private final int m_selector_height;
	private final Paint paint = new Paint();
	private final Paint paint_selector = new Paint();
	
	public LevelSelector(int x, int y, int width, int height, int actionId) {
		super(x, y, width, height, actionId);
		m_x_offset = (int) (m_width * 0.35);
		m_selector_height = (int) (m_height * 0.3);
	}
	
	public int getLevel() {
		return m_position;
	}
	
	public void reset() {
		m_position = STARTVALUE;
	}
	
	public boolean moveHandle(float x, float y) {
		if ((x >= m_x) && (x <= (m_x + m_width))) {
			if (y <= m_y) {
				m_position = 100;
			} else if (y >= m_y + m_height) {
				m_position = 0;
			} else {
				m_position = 100 - (int) (((y - m_y) / m_height) * 100);
			}
		}
		return true;
	}

	public void doDraw(Canvas canvas) {
		int selector_pos = (m_height - m_selector_height) - ((int) ((m_height - m_selector_height) * ((double)m_position / 100)));
		
		if (m_active) {
			paint.setColor(Color.LTGRAY);
		} else {
			paint.setColor(Color.DKGRAY);
		}
		paint.setStyle(Style.FILL);
		
		paint_selector.setColor(Color.DKGRAY);
		paint_selector.setStyle(Style.FILL);
	
		canvas.drawRect(m_x + m_x_offset, m_y, m_x + m_width - (m_x_offset), m_y + m_height, paint);
		canvas.drawRect(m_x, m_y + selector_pos, m_x + m_width, m_y + selector_pos + m_selector_height, paint_selector);
	}
}
