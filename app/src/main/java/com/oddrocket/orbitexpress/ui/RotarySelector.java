package com.oddrocket.orbitexpress.ui;

import com.oddrocket.orbitexpress.gfw.Collision;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class RotarySelector extends UiComponent {
	private static final int STARTVALUE = 0;
	private int m_angle = STARTVALUE;
	private final int x_offset;
	private final int y_offset;
	private final int cx;
	private final int cy;
	private final Paint paint = new Paint();
	private final Paint paint_selector = new Paint();
	
	public RotarySelector(int x, int y, int width, int height, int actionId) {
		super(x, y, width, height, actionId);
		
		x_offset = m_width / 2;
		y_offset = m_height / 2;
		cx = m_x + x_offset;
		cy = m_y + y_offset;
	}
	
	public int getAngle() {
		return m_angle;
	}
	
	public void reset() {
		m_angle = STARTVALUE;
	}
	
	public boolean clickHandle(float x, float y) {
		if (!m_active) {
			return false;
		}
		float targetWidth = (float) (m_width * 1.3);
		float targetHeight = (float) (m_height * 1.3);
		return Collision.pointCollision(x, y, m_x, m_y, targetWidth, targetHeight);
	}
	
	public boolean moveHandle(float x, float y) {
		m_angle = (int) Math.toDegrees(Math.atan2(x - cx, cy - y));
		return true;
	}

	public void doDraw(Canvas canvas) {
		int radius_selector = (int) (x_offset * 0.4);
		int radius_outer = x_offset;
		int radius_inner = (int) (radius_outer * 0.8);
		int radius_selector_calc = (int) (radius_outer * 0.9);
		int outer_width = radius_outer - radius_inner;
		radius_outer = radius_outer - (outer_width/2);
		
		if (m_active) {
			paint.setColor(Color.LTGRAY);
		} else {
			paint.setColor(Color.DKGRAY);
		}
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(outer_width);
		
		paint_selector.setColor(Color.DKGRAY);
		paint_selector.setStyle(Style.FILL);
		
		canvas.drawCircle(m_x + x_offset, m_y + y_offset, radius_outer, paint);
		
		/* XXX: angle -90? */
		double sx = cx + (radius_selector_calc * Math.cos(Math.toRadians(m_angle - 90)));
		double sy = cy + (radius_selector_calc * Math.sin(Math.toRadians(m_angle - 90))); 
		canvas.drawCircle((float)sx, (float)sy, radius_selector, paint_selector);
	}
}
