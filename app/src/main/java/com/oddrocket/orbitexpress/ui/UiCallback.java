package com.oddrocket.orbitexpress.ui;

public interface UiCallback {
	void uiAction(int action, int id, int value);
}
