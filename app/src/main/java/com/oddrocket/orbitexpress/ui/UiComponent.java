package com.oddrocket.orbitexpress.ui;

import com.oddrocket.orbitexpress.gfw.Collision;

import android.graphics.Canvas;

class UiComponent {
	final int m_x;
	final int m_y;
	final int m_width;
	final int m_height;
	int m_actionId;
	boolean m_active = true;
	
	UiComponent(int x, int y, int width, int height, int actionId) {
		m_x = x;
		m_y = y;
		m_width = width;
		m_height = height;
		m_actionId = actionId;
	}
	
	public boolean clickHandle(float x, float y) {
		return m_active && Collision.pointCollision(x, y, m_x, m_y, m_width, m_height);
	}
	
	public boolean moveHandle(float x, float y) {
		return m_active && Collision.pointCollision(x, y, m_x, m_y, m_width, m_height);
	}
	
	public int getActionId() {
		return m_actionId;
	}
	
	public void doDraw(Canvas canvas) {
	}
	
	public void setInactive() {
		m_active = false;
	}
	
	public void setActive() {
		m_active = true;
	}
}
