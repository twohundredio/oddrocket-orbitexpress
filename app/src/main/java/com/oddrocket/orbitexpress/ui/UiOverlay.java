package com.oddrocket.orbitexpress.ui;

import java.util.Vector;

import android.graphics.Canvas;

public class UiOverlay {
	private final Vector<UiComponent> m_elements = new Vector<>();
	private UiCallback m_callback;
	private UiComponent m_active = null;

	public void addComponent(UiComponent component) {
		m_elements.add(component);
	}
	
	public void setCallback(UiCallback callback) {
		m_callback = callback;
	}
	
	private void cancel() {
		m_active = null;
	}
	
	public void doDraw(Canvas canvas) {
		for (UiComponent e : m_elements) {
			e.doDraw(canvas);
		}
	}
	
	public boolean downHandle(float x, float y) {
		for (UiComponent e : m_elements) {
			if(e.clickHandle(x, y)) {
				m_active = e;
				return true;
			}
		}
		return false;
	}
	
	public boolean moveHandle(float x, float y) {
		if (m_active != null) {
			if (m_active.moveHandle(x, y)) {
				return true;
			}
			m_active = null;
		}
		return false;
	}
	
	public boolean upHandle(float x, float y) {
		boolean canHandle = false;
		if (m_active != null) {
			if(m_active.clickHandle(x, y)) {
				canHandle = true;
				m_callback.uiAction(0, m_active.getActionId(), 0);
			}
		}
		m_active = null;
		return canHandle;
	}
	
	public void setAllInactive() {
		cancel();
		for (UiComponent e : m_elements) {
			e.setInactive();
		}
	}
	
	public void setAllActive() {
		for (UiComponent e : m_elements) {
			e.setActive();
		}
	}
}
